﻿namespace HotelManagementSystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPregledSoba = new System.Windows.Forms.Button();
            this.btnRezervacija = new System.Windows.Forms.Button();
            this.btnRadnici = new System.Windows.Forms.Button();
            this.btnPregledGostiju = new System.Windows.Forms.Button();
            this.btnPregledBazena = new System.Windows.Forms.Button();
            this.btnPregledRestorana = new System.Windows.Forms.Button();
            this.btnPregledSale = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnIzadji = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPregledSoba
            // 
            this.btnPregledSoba.FlatAppearance.BorderSize = 0;
            this.btnPregledSoba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledSoba.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPregledSoba.Location = new System.Drawing.Point(2, 192);
            this.btnPregledSoba.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPregledSoba.Name = "btnPregledSoba";
            this.btnPregledSoba.Size = new System.Drawing.Size(222, 62);
            this.btnPregledSoba.TabIndex = 0;
            this.btnPregledSoba.Text = "Pregled sobe";
            this.btnPregledSoba.UseVisualStyleBackColor = true;
            this.btnPregledSoba.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRezervacija
            // 
            this.btnRezervacija.FlatAppearance.BorderSize = 0;
            this.btnRezervacija.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRezervacija.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnRezervacija.Location = new System.Drawing.Point(2, 2);
            this.btnRezervacija.Name = "btnRezervacija";
            this.btnRezervacija.Size = new System.Drawing.Size(222, 62);
            this.btnRezervacija.TabIndex = 1;
            this.btnRezervacija.Text = "Rezervacija";
            this.btnRezervacija.UseVisualStyleBackColor = true;
            this.btnRezervacija.UseWaitCursor = true;
            this.btnRezervacija.Click += new System.EventHandler(this.btnRezervacija_Click);
            // 
            // btnRadnici
            // 
            this.btnRadnici.FlatAppearance.BorderSize = 0;
            this.btnRadnici.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRadnici.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRadnici.Location = new System.Drawing.Point(2, 73);
            this.btnRadnici.Name = "btnRadnici";
            this.btnRadnici.Size = new System.Drawing.Size(222, 54);
            this.btnRadnici.TabIndex = 2;
            this.btnRadnici.Text = "Upravljaj radnicima";
            this.btnRadnici.UseVisualStyleBackColor = true;
            this.btnRadnici.Click += new System.EventHandler(this.btnRadnici_Click);
            // 
            // btnPregledGostiju
            // 
            this.btnPregledGostiju.FlatAppearance.BorderSize = 0;
            this.btnPregledGostiju.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledGostiju.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPregledGostiju.Location = new System.Drawing.Point(2, 136);
            this.btnPregledGostiju.Name = "btnPregledGostiju";
            this.btnPregledGostiju.Size = new System.Drawing.Size(222, 52);
            this.btnPregledGostiju.TabIndex = 3;
            this.btnPregledGostiju.Text = "Pregled gostiju";
            this.btnPregledGostiju.UseVisualStyleBackColor = true;
            this.btnPregledGostiju.Click += new System.EventHandler(this.btnPregledGostiju_Click);
            // 
            // btnPregledBazena
            // 
            this.btnPregledBazena.FlatAppearance.BorderSize = 0;
            this.btnPregledBazena.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledBazena.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPregledBazena.Location = new System.Drawing.Point(2, 328);
            this.btnPregledBazena.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPregledBazena.Name = "btnPregledBazena";
            this.btnPregledBazena.Size = new System.Drawing.Size(222, 51);
            this.btnPregledBazena.TabIndex = 6;
            this.btnPregledBazena.Text = "Pregled bazena";
            this.btnPregledBazena.UseVisualStyleBackColor = true;
            this.btnPregledBazena.Click += new System.EventHandler(this.btnPregledBazena_Click);
            // 
            // btnPregledRestorana
            // 
            this.btnPregledRestorana.FlatAppearance.BorderSize = 0;
            this.btnPregledRestorana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledRestorana.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPregledRestorana.Location = new System.Drawing.Point(2, 389);
            this.btnPregledRestorana.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPregledRestorana.Name = "btnPregledRestorana";
            this.btnPregledRestorana.Size = new System.Drawing.Size(222, 52);
            this.btnPregledRestorana.TabIndex = 5;
            this.btnPregledRestorana.Text = "Pregled restorana";
            this.btnPregledRestorana.UseVisualStyleBackColor = true;
            this.btnPregledRestorana.Click += new System.EventHandler(this.btnPregledRestorana_Click);
            // 
            // btnPregledSale
            // 
            this.btnPregledSale.FlatAppearance.BorderSize = 0;
            this.btnPregledSale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledSale.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPregledSale.Location = new System.Drawing.Point(2, 264);
            this.btnPregledSale.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPregledSale.Name = "btnPregledSale";
            this.btnPregledSale.Size = new System.Drawing.Size(222, 54);
            this.btnPregledSale.TabIndex = 4;
            this.btnPregledSale.Text = "Pregled sale";
            this.btnPregledSale.UseVisualStyleBackColor = true;
            this.btnPregledSale.Click += new System.EventHandler(this.btnPregledSale_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotelManagementSystem.Properties.Resources.pocetna;
            this.pictureBox1.Location = new System.Drawing.Point(230, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(726, 501);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // btnIzadji
            // 
            this.btnIzadji.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnIzadji.FlatAppearance.BorderSize = 0;
            this.btnIzadji.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzadji.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIzadji.Location = new System.Drawing.Point(2, 451);
            this.btnIzadji.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnIzadji.Name = "btnIzadji";
            this.btnIzadji.Size = new System.Drawing.Size(222, 52);
            this.btnIzadji.TabIndex = 8;
            this.btnIzadji.Text = "Izadji";
            this.btnIzadji.UseVisualStyleBackColor = true;
            this.btnIzadji.Click += new System.EventHandler(this.btnIzadji_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(55)))));
            this.CancelButton = this.btnIzadji;
            this.ClientSize = new System.Drawing.Size(962, 513);
            this.Controls.Add(this.btnIzadji);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnPregledBazena);
            this.Controls.Add(this.btnPregledRestorana);
            this.Controls.Add(this.btnPregledSale);
            this.Controls.Add(this.btnPregledGostiju);
            this.Controls.Add(this.btnRadnici);
            this.Controls.Add(this.btnRezervacija);
            this.Controls.Add(this.btnPregledSoba);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPregledSoba;
        private System.Windows.Forms.Button btnRezervacija;
        private System.Windows.Forms.Button btnRadnici;
        private System.Windows.Forms.Button btnPregledGostiju;
        private System.Windows.Forms.Button btnPregledBazena;
        private System.Windows.Forms.Button btnPregledRestorana;
        private System.Windows.Forms.Button btnPregledSale;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnIzadji;
    }
}

