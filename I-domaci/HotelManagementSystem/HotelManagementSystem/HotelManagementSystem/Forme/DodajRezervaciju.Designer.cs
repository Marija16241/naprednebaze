﻿namespace HotelManagementSystem.Forme
{
    partial class DodajRezervaciju
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label3;
            this.dgvProstorije = new System.Windows.Forms.DataGridView();
            this.btnDodajGoste = new System.Windows.Forms.Button();
            this.lvGosti = new System.Windows.Forms.ListView();
            this.Ime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Prezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ValidacioniDokument = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblOznakaProstorije = new System.Windows.Forms.Label();
            this.btnZapamtiRezervaciju = new System.Windows.Forms.Button();
            this.btnIzracunajCenu = new System.Windows.Forms.Button();
            this.lblCena = new System.Windows.Forms.Label();
            this.dtpDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtpDatumOd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProstorija = new System.Windows.Forms.Label();
            this.cmbProstorija = new System.Windows.Forms.ComboBox();
            this.btnIzadji = new System.Windows.Forms.Button();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProstorije)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(68, 333);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(50, 20);
            label3.TabIndex = 25;
            label3.Text = "Gosti:";
            // 
            // dgvProstorije
            // 
            this.dgvProstorije.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(55)))));
            this.dgvProstorije.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProstorije.GridColor = System.Drawing.Color.White;
            this.dgvProstorije.Location = new System.Drawing.Point(68, 161);
            this.dgvProstorije.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgvProstorije.Name = "dgvProstorije";
            this.dgvProstorije.RowHeadersWidth = 51;
            this.dgvProstorije.RowTemplate.Height = 24;
            this.dgvProstorije.Size = new System.Drawing.Size(356, 152);
            this.dgvProstorije.TabIndex = 32;
            this.dgvProstorije.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProstorije_CellContentClick);
            // 
            // btnDodajGoste
            // 
            this.btnDodajGoste.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajGoste.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDodajGoste.Location = new System.Drawing.Point(138, 329);
            this.btnDodajGoste.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnDodajGoste.Name = "btnDodajGoste";
            this.btnDodajGoste.Size = new System.Drawing.Size(286, 24);
            this.btnDodajGoste.TabIndex = 31;
            this.btnDodajGoste.Text = "Dodaj goste";
            this.btnDodajGoste.UseVisualStyleBackColor = true;
            this.btnDodajGoste.Click += new System.EventHandler(this.btnDodajGoste_Click);
            // 
            // lvGosti
            // 
            this.lvGosti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(55)))));
            this.lvGosti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Ime,
            this.Prezime,
            this.ValidacioniDokument});
            this.lvGosti.ForeColor = System.Drawing.Color.White;
            this.lvGosti.FullRowSelect = true;
            this.lvGosti.GridLines = true;
            this.lvGosti.HideSelection = false;
            this.lvGosti.Location = new System.Drawing.Point(70, 379);
            this.lvGosti.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvGosti.Name = "lvGosti";
            this.lvGosti.Size = new System.Drawing.Size(354, 121);
            this.lvGosti.TabIndex = 30;
            this.lvGosti.UseCompatibleStateImageBehavior = false;
            this.lvGosti.View = System.Windows.Forms.View.Details;
            // 
            // Ime
            // 
            this.Ime.Text = "Ime";
            // 
            // Prezime
            // 
            this.Prezime.Text = "Prezime";
            this.Prezime.Width = 98;
            // 
            // ValidacioniDokument
            // 
            this.ValidacioniDokument.Text = "Validacioni dokument";
            this.ValidacioniDokument.Width = 150;
            // 
            // lblOznakaProstorije
            // 
            this.lblOznakaProstorije.AutoSize = true;
            this.lblOznakaProstorije.Location = new System.Drawing.Point(68, 278);
            this.lblOznakaProstorije.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOznakaProstorije.Name = "lblOznakaProstorije";
            this.lblOznakaProstorije.Size = new System.Drawing.Size(185, 20);
            this.lblOznakaProstorije.TabIndex = 29;
            this.lblOznakaProstorije.Text = "Pisace oznaka prostorije";
            // 
            // btnZapamtiRezervaciju
            // 
            this.btnZapamtiRezervaciju.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZapamtiRezervaciju.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnZapamtiRezervaciju.Location = new System.Drawing.Point(70, 573);
            this.btnZapamtiRezervaciju.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnZapamtiRezervaciju.Name = "btnZapamtiRezervaciju";
            this.btnZapamtiRezervaciju.Size = new System.Drawing.Size(210, 50);
            this.btnZapamtiRezervaciju.TabIndex = 28;
            this.btnZapamtiRezervaciju.Text = "Zapamti rezervaciju";
            this.btnZapamtiRezervaciju.UseVisualStyleBackColor = true;
            this.btnZapamtiRezervaciju.Click += new System.EventHandler(this.btnZapamtiRezervaciju_Click);
            // 
            // btnIzracunajCenu
            // 
            this.btnIzracunajCenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzracunajCenu.Location = new System.Drawing.Point(68, 522);
            this.btnIzracunajCenu.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnIzracunajCenu.Name = "btnIzracunajCenu";
            this.btnIzracunajCenu.Size = new System.Drawing.Size(110, 30);
            this.btnIzracunajCenu.TabIndex = 27;
            this.btnIzracunajCenu.Text = "Izracunaj cenu:";
            this.btnIzracunajCenu.UseVisualStyleBackColor = true;
            this.btnIzracunajCenu.Click += new System.EventHandler(this.btnIzracunajCenu_Click);
            // 
            // lblCena
            // 
            this.lblCena.AutoSize = true;
            this.lblCena.Location = new System.Drawing.Point(208, 527);
            this.lblCena.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCena.Name = "lblCena";
            this.lblCena.Size = new System.Drawing.Size(0, 20);
            this.lblCena.TabIndex = 26;
            // 
            // dtpDatumDo
            // 
            this.dtpDatumDo.Location = new System.Drawing.Point(180, 70);
            this.dtpDatumDo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dtpDatumDo.Name = "dtpDatumDo";
            this.dtpDatumDo.Size = new System.Drawing.Size(244, 26);
            this.dtpDatumDo.TabIndex = 24;
            // 
            // dtpDatumOd
            // 
            this.dtpDatumOd.Location = new System.Drawing.Point(180, 21);
            this.dtpDatumOd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dtpDatumOd.Name = "dtpDatumOd";
            this.dtpDatumOd.Size = new System.Drawing.Size(244, 26);
            this.dtpDatumOd.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Datum do:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 20;
            this.label1.Text = "Datum od:";
            // 
            // lblProstorija
            // 
            this.lblProstorija.AutoSize = true;
            this.lblProstorija.Location = new System.Drawing.Point(68, 116);
            this.lblProstorija.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProstorija.Name = "lblProstorija";
            this.lblProstorija.Size = new System.Drawing.Size(200, 20);
            this.lblProstorija.TabIndex = 19;
            this.lblProstorija.Text = "Prikazi slobodne prostorije:";
            // 
            // cmbProstorija
            // 
            this.cmbProstorija.FormattingEnabled = true;
            this.cmbProstorija.Location = new System.Drawing.Point(284, 113);
            this.cmbProstorija.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbProstorija.Name = "cmbProstorija";
            this.cmbProstorija.Size = new System.Drawing.Size(140, 28);
            this.cmbProstorija.TabIndex = 18;
            this.cmbProstorija.SelectedIndexChanged += new System.EventHandler(this.cmbProstorija_SelectedIndexChanged);
            // 
            // btnIzadji
            // 
            this.btnIzadji.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzadji.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnIzadji.Location = new System.Drawing.Point(296, 573);
            this.btnIzadji.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnIzadji.Name = "btnIzadji";
            this.btnIzadji.Size = new System.Drawing.Size(140, 50);
            this.btnIzadji.TabIndex = 33;
            this.btnIzadji.Text = "Izadji";
            this.btnIzadji.UseVisualStyleBackColor = true;
            this.btnIzadji.Click += new System.EventHandler(this.btnIzadji_Click);
            // 
            // DodajRezervaciju
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(55)))));
            this.ClientSize = new System.Drawing.Size(502, 653);
            this.Controls.Add(this.btnIzadji);
            this.Controls.Add(this.dgvProstorije);
            this.Controls.Add(this.btnDodajGoste);
            this.Controls.Add(this.lvGosti);
            this.Controls.Add(this.lblOznakaProstorije);
            this.Controls.Add(this.btnZapamtiRezervaciju);
            this.Controls.Add(this.btnIzracunajCenu);
            this.Controls.Add(this.lblCena);
            this.Controls.Add(label3);
            this.Controls.Add(this.dtpDatumDo);
            this.Controls.Add(this.dtpDatumOd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblProstorija);
            this.Controls.Add(this.cmbProstorija);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "DodajRezervaciju";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DodajRezervaciju";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DodajRezervaciju_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProstorije)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProstorije;
        private System.Windows.Forms.Button btnDodajGoste;
        private System.Windows.Forms.ListView lvGosti;
        private System.Windows.Forms.ColumnHeader Ime;
        private System.Windows.Forms.ColumnHeader Prezime;
        private System.Windows.Forms.ColumnHeader ValidacioniDokument;
        private System.Windows.Forms.Label lblOznakaProstorije;
        private System.Windows.Forms.Button btnZapamtiRezervaciju;
        private System.Windows.Forms.Button btnIzracunajCenu;
        private System.Windows.Forms.Label lblCena;
        private System.Windows.Forms.DateTimePicker dtpDatumDo;
        private System.Windows.Forms.DateTimePicker dtpDatumOd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblProstorija;
        private System.Windows.Forms.ComboBox cmbProstorija;
        private System.Windows.Forms.Button btnIzadji;
    }
}