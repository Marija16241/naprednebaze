﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
    public class Zahtev
    {
        public ObjectId id { get; set; }
        public string tip { get; set; }
    }
}
