﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
    public class ZahteviPredavanje : Zahtev
    {
        public string predmet { get; set; }
        public string vreme { get; set; }
        public string mesto { get; set; }
        public List<String> oznake { get; set; }
        public MongoDBRef predavac { get; set; }

        public ZahteviPredavanje()
        {
            oznake = new List<string>();
        }


    }
}
