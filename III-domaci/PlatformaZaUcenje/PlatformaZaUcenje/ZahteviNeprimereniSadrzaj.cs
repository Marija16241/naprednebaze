﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
    public class ZahteviNeprimereniSadrzaj : Zahtev
    {
        public string opis { get; set; }
        public MongoDBRef dokument { get; set; }
       
    }
}
