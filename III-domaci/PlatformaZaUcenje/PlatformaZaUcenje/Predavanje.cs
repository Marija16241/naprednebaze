﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
   public class Predavanje
    {
        public ObjectId id { get; set; }
        public string predmet { get; set; }
        public string vreme { get; set; }
        public string mesto { get; set; }
        public string ocena { get; set; }
        public List<String> oznake { get; set; }
        public List<MongoDBRef> prijevljeniKorisnici { get; set; }

        public Predavanje()
        {
            prijevljeniKorisnici = new List<MongoDBRef>();
            oznake = new List<string>();
        }
    }
}
