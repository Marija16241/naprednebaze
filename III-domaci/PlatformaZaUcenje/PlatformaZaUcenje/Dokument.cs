﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
    public class Dokument
    {
        public ObjectId id { get; set; } 
        public string predmet { get; set; }
        public string link { get; set; }
        public string format { get; set; }
        public string tip { get; set; }
        public string ocena { get; set; }
        public List<string> oznake { get; set; }

    }
}
