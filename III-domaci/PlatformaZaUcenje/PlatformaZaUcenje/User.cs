﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;


namespace PlatformaZaUcenje
{
    public class User
    {
        public ObjectId id { get; set; }
       
        public string email { get; set; }
        public string sifra { get; set; }
        
    }
}
