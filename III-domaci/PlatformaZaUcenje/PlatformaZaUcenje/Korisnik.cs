﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PlatformaZaUcenje
{
   public class Korisnik : User
    {
        public string ime { get; set; }
        public string prezime { get; set; }
        public string telefon { get; set; }
        public List<MongoDBRef> dokumenta { get; set; } //ono sto je on postavio
       
        public List<MongoDBRef> predavanja { get; set; }

        public Korisnik()
        {
           
            dokumenta = new List<MongoDBRef>();
            predavanja = new List<MongoDBRef>();
        }
    }
}
