﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformaZaUcenje
{
    public class ZahteviLiteratura : Zahtev
    {
        public string poruka { get; set; }
        public string predmet { get; set; }
    }
}
