﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public class Admin : User
    {
        public List<MongoDBRef> korisnici { get; set; }
   

        public Admin()
        {
         
            korisnici = new List<MongoDBRef>();
        
        }

    }
}
