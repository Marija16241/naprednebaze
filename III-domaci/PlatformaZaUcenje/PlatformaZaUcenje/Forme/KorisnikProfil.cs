﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
namespace PlatformaZaUcenje
{
    public partial class KorisnikProfil : Form
    {
        Korisnik korisnik;
        public KorisnikProfil()
        {
            InitializeComponent();
        }
        public KorisnikProfil(Korisnik k)
        {
            korisnik = k;
            InitializeComponent();
        }

        private void btnDokumenta_Click(object sender, EventArgs e)
        {
            KPrikazDokumenata kpd = new KPrikazDokumenata();
            kpd.Show();
        }

        private void btnSadrzaj_Click(object sender, EventArgs e)
        {

            KorisnikMojSadrzaj kms = new KorisnikMojSadrzaj(korisnik);
            kms.Show();
        }

        private void btnZahtevi_Click(object sender, EventArgs e)
        {
            ZahtevLiteratura zl = new ZahtevLiteratura();
            zl.ShowDialog();
        }

        private void btnPredavanja_Click(object sender, EventArgs e)
        {
            KorisnikPredavanja kp = new KorisnikPredavanja(korisnik);
            kp.ShowDialog();
        }

        private void btnProfil_Click(object sender, EventArgs e)
        {
            ProfilKorisnikIzmeni pk = new ProfilKorisnikIzmeni(korisnik);
           // pk.Show();
            if (pk.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string connectionString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connectionString);
                var db = server.GetDatabase("platformazaucenje");

                var query = Query.EQ("email", korisnik.email);
                var collection = db.GetCollection<Korisnik>("korisnik");
                Korisnik a = collection.Find(query).FirstOrDefault();
                korisnik = a;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
