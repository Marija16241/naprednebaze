﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
namespace PlatformaZaUcenje
{
    public partial class PrijaviNeprimerenSadrzaj : Form
    {
        private ObjectId id;
        public PrijaviNeprimerenSadrzaj()
        {
            InitializeComponent();
        }
        public PrijaviNeprimerenSadrzaj(ObjectId i)
        {
            InitializeComponent();
            id = i;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZahteviNeprimereniSadrzaj z = new ZahteviNeprimereniSadrzaj();
            z.opis = richTextBox1.Text;
            z.tip = "neprimeren";
            z.dokument=new MongoDBRef("dokument", id);
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var Collection = db.GetCollection<Zahtev>("zahtev"); Collection.Insert(z);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PrijaviNeprimerenSadrzaj_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
