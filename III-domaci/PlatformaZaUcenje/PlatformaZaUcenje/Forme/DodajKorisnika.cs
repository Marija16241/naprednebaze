﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class DodajKorisnika : Form
    {
        public AdminKorisnici ak;
        public Admin a;
        public DodajKorisnika()
        {
            InitializeComponent();
        }
        public DodajKorisnika(AdminKorisnici ak,Admin a)
        {
            InitializeComponent();
            this.a = a;
            this.ak = ak;
        }

        private void DodajKorisnika_Load(object sender, EventArgs e)
        {

        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            Korisnik korisnik = new Korisnik { ime = txtIme.Text,
                prezime = txtPrezime.Text,
                telefon = txtTelefon.Text,
                email = txtEmail.Text,
                sifra = txtSifra.Text,
                dokumenta = new List<MongoDBRef>(),
                predavanja = new List<MongoDBRef>()
            };
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            //db.CreateCollection("preduzece");

            var collection = db.GetCollection<Korisnik>("korisnik");
            collection.Insert(korisnik);
            a.korisnici.Add(new MongoDBRef("korisnik", korisnik.id));
            collection.Save(a);
         
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DodajKorisnika_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
