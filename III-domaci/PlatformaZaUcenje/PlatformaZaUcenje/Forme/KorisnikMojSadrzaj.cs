﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class KorisnikMojSadrzaj : Form
    {
        Korisnik korisnik = new Korisnik();
        List<Dokument> listaDokumenata = new List<Dokument>();
        public KorisnikMojSadrzaj()
        {
            InitializeComponent();
        }

        public KorisnikMojSadrzaj(Korisnik k)
        {
            InitializeComponent();
            korisnik = k;
        }

        private void KorisnikMojSadrzaj_Load(object sender, EventArgs e)
        {
            unesiDataGrid();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajDokument dd = new DodajDokument(korisnik);
            dd.ShowDialog();
            unesiDataGrid();

        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            int selektovani = dgv.CurrentRow.Index;
            //MessageBox.Show(selektovani.ToString());
            if (selektovani < 0)
            {
                MessageBox.Show("Nijedan dokument nije selektovan");
                return;
            }

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            //provera zahteva
            var collectionZahtevi = db.GetCollection("zahtev");
            var zahtevi = from zahtev in collectionZahtevi.AsQueryable<ZahteviNeprimereniSadrzaj>()
                          where zahtev.tip == "neprimeren"
                          select zahtev;
            //var zahtevi = collectionZahtevi.FindAll();
            foreach (ZahteviNeprimereniSadrzaj z in zahtevi)
            {
                if (z.dokument == new MongoDBRef("dokument", listaDokumenata.ElementAt(selektovani).id))
                {
                    MessageBox.Show("Nije moguce obrisati dokument za koji je poslato obavestenje adminu o neprimerenom sadrzaju.");
                    return;
                }
            }

            //brisanje dokumenta
            var collectionDokument = db.GetCollection<Dokument>("dokument");
            var query = Query.EQ("_id", listaDokumenata.ElementAt(selektovani).id);
            collectionDokument.Remove(query);

            var collection = db.GetCollection<Korisnik>("korisnik");
            korisnik = collection.Find(Query.EQ("email", korisnik.email)).First();
            korisnik.dokumenta.RemoveAt(selektovani);
            collection.Save(korisnik);


            //azuriranje datagrida
            unesiDataGrid();

        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            int selektovani = dgv.CurrentRow.Index;
            if (selektovani < 0)
            {
                MessageBox.Show("Nijedan dokument nije selektovan");
                return;
            }

            AzurirajDokument ad = new AzurirajDokument(korisnik.dokumenta.ElementAt(selektovani));
            ad.ShowDialog();
            unesiDataGrid();
        }

        void unesiDataGrid()
        {
            listaDokumenata.Clear();
            dgv.Rows.Clear();
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            var collectionKorisnik = db.GetCollection<Korisnik>("korisnik");
            korisnik = collectionKorisnik.Find(Query.EQ("email", korisnik.email)).First();

            var collection1 = db.GetCollection<Dokument>("dokument");
            MongoCursor<Dokument> dokument = collection1.FindAll();

            var sviBlanketi = from blanket in collection1.AsQueryable<Blanket>()
                              where blanket.tip == "blanket"
                              select blanket;
            var sveKnjige = from knjiga in collection1.AsQueryable<Knjiga>()
                            where knjiga.tip == "knjiga"
                            select knjiga;
            if (korisnik.dokumenta == null)
                return;
            foreach (MongoDBRef d in korisnik.dokumenta)
            {
                //MessageBox.Show(d.Id.ToString());
                var dokBlanketi = from blanket in sviBlanketi.AsQueryable<Blanket>()
                                  where d.Id.AsObjectId == blanket.id
                                  select blanket;
                var dokKnjige = from knjiga in sveKnjige.AsQueryable<Knjiga>()
                                where d.Id.AsObjectId == knjiga.id
                                select knjiga;


                foreach (Blanket b in dokBlanketi)
                {
                    listaDokumenata.Add(b);
                    String red = "TIP: " + b.tip + " PREDMET: " + b.predmet + " LINK: " + b.link + "  ";

                    if (b.rok != "")
                        red += "ISPITNI ROK: " + b.rok + " ";
                    if (b.godina != "")
                        red += "GODINA: " + b.godina + " ";
                    if (b.tipBlanketa != "")
                        red += "TIP BLANKETA: " + b.tipBlanketa + " ";
                    if (b.ocena != null)
                        red += "OCENA: " + b.ocena + " ";
                    if (b.oznake.Count > 0)
                    {
                        red += "DODATNE KARAKTERISTIKE:";
                        foreach (String s in b.oznake)
                            red += " " + s;
                    }
                    dgv.Rows.Add(red);
                }
                foreach (Knjiga k in dokKnjige)
                {
                    listaDokumenata.Add(k);

                    String red = "TIP: " + k.tip + " PREDMET: " + k.predmet + " LINK: " + k.link + "  ";

                    if (k.autor != "")
                        red += "AUTOR: " + k.autor + " ";
                    if (k.opis != "")
                        red += "OPIS: " + k.opis + " ";
                    if (k.ocena != null)
                        red += "OCENA: " + k.ocena + " ";
                    if (k.oznake.Count > 0)
                    {
                        red += "DODATNE KARAKTERISTIKE:";
                        foreach (String s in k.oznake)
                            red += " " + s;
                    }


                    dgv.Rows.Add(red);
                }
            }
            }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void KorisnikMojSadrzaj_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
