﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class DodajDokument : Form
    {
        Korisnik korisnik = new Korisnik();
        public DodajDokument()
        {
            InitializeComponent();
            panel.Visible = false;
            panel1.Visible = false;
        }

        public DodajDokument(Korisnik k)
        {
            InitializeComponent();
            korisnik = k;
            //MessageBox.Show(korisnik.email);
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (cmbTip.SelectedIndex < 0)
            {
                MessageBox.Show("Morate izabrati tip dokumenta.");
                return;
            }
            //MessageBox.Show(cmbTip.SelectedItem.ToString());
            if (cmbTip.SelectedItem.ToString().Equals("blanket"))
            {
                //rok, godina i tip blanketa
                
                String godina, tip;

                if (cmbGodina.SelectedIndex < 0)
                    godina = "";
                else
                    godina = cmbGodina.SelectedItem.ToString();

                if (cmbTipBlanketa.SelectedIndex < 0)
                    tip = "";
                else
                    tip = cmbTipBlanketa.SelectedItem.ToString();

                unesiBlanket(txtRok.Text, godina, tip);
                    
            }
            else
            {
                String autor, opis;
                unesiKnjigu(txtAutor.Text, txtOpis.Text);

            }

            /* 

             List<String> o = new List<string>();
             foreach (DataGridViewRow item in dgvPodaci.Rows)
             {
                 if(item.Cells[0].Value != null)
                 {
                     o.Add(item.Cells[0].Value.ToString());
                 }

             }
             if(txtPredmet.Text == null)
             {
                 MessageBox.Show("Neophodno je uneti naziv predmeta.");
                 return;
             }
             Dokument d = new Dokument { naziv = txtNaziv.Text, predmet = txtPredmet.Text, link = txtLink.Text, format = txtFormat.Text, oznake = o };
             collection.Insert(d);

            // MessageBox.Show(d.id.ToString());
             var collectionKorisnik = db.GetCollection<Korisnik>("korisnik");
             MessageBox.Show(korisnik.email);
             var query = Query.EQ("email", korisnik.email);

             // var update = MongoDB.Driver.Builders.Update.Set("dokument", BsonValue.Create(new Dokument {id = d.id, naziv = d.naziv}));
             ////var update = MongoDB.Driver.Builders.Update.Set("dokument", BsonValue.Create(new List<MongoDBRef> { d.id }));

             // collection1.Update(query, update);
             var kl = collectionKorisnik.Find(query);
             foreach (Korisnik k in kl)
             {
                 k.dokument.Add(new Dokument { id = d.id, naziv = d.naziv });
                 //k.dokument.Add(new List<String> { d.id.ToString(), d.naziv});
                 collectionKorisnik.Save(k);
             }

             this.Close();
             */
            this.Close();
        }

        void unesiBlanket(String ispitniRok, String god, String tipB)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<Dokument>("dokument");

            List<String> o = new List<string>();
            foreach (DataGridViewRow item in dgvPodaci.Rows)
            {
                if (item.Cells[0].Value != null)
                {
                    o.Add(item.Cells[0].Value.ToString());
                }

            }
            if (txtPredmet.Text == null || txtLink.Text == null || txtFormat.Text == null)
            {
                MessageBox.Show("Neophodno je uneti naziv, link i format predmeta.");
                return;
            }
            Blanket d = new Blanket { tip = cmbTip.SelectedItem.ToString(), predmet = txtPredmet.Text, link = txtLink.Text, format = txtFormat.Text, oznake = o, rok = ispitniRok, tipBlanketa = tipB, godina = god };
            collection.Insert(d);


            korisnik.dokumenta.Add(new MongoDBRef("blanket", d.id));
            var collectionKorisnici = db.GetCollection<Korisnik>("korisnik");
            collectionKorisnici.Save(korisnik);

    
        }

        void unesiKnjigu(String autorKnjige, String opisKnjige)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<Dokument>("dokument");

            List<String> o = new List<string>();
            foreach (DataGridViewRow item in dgvPodaci.Rows)
            {
                if (item.Cells[0].Value != null)
                {
                    o.Add(item.Cells[0].Value.ToString());
                }

            }
            if (txtPredmet.Text == null || txtLink.Text == null || txtFormat.Text == null)
            {
                MessageBox.Show("Neophodno je uneti naziv, link i format predmeta.");
                return;
            }
            Knjiga d = new Knjiga { tip = cmbTip.SelectedItem.ToString(), predmet = txtPredmet.Text, link = txtLink.Text, format = txtFormat.Text, oznake = o, autor = autorKnjige, opis = opisKnjige};
            collection.Insert(d);

            korisnik.dokumenta.Add(new MongoDBRef("knjiga", d.id));

            //dodavanje dokumenta korisniku
            var collectionKorisnik = db.GetCollection<Korisnik>("korisnik");
            collectionKorisnik.Save(korisnik);
            
        }

        private void cmbTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbTip.SelectedItem.Equals("knjiga"))
            { 
                panel.Enabled = false;
                panel1.Enabled = true;
            }
            else if(cmbTip.SelectedItem.Equals("blanket"))
            {
                panel1.Enabled = false;
                panel.Enabled = true;
            }
        }

        private void DodajDokument_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
