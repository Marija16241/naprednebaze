﻿namespace PlatformaZaUcenje
{
    partial class ZahtevLiteratura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvZahtevi = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPoruka = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnZatrazi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPredmet = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZahtevi)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvZahtevi
            // 
            this.dgvZahtevi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvZahtevi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvZahtevi.Location = new System.Drawing.Point(39, 51);
            this.dgvZahtevi.Name = "dgvZahtevi";
            this.dgvZahtevi.RowHeadersWidth = 51;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvZahtevi.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvZahtevi.RowTemplate.Height = 24;
            this.dgvZahtevi.Size = new System.Drawing.Size(610, 326);
            this.dgvZahtevi.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Zahtevana literatura";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // txtPoruka
            // 
            this.txtPoruka.Location = new System.Drawing.Point(42, 540);
            this.txtPoruka.Multiline = true;
            this.txtPoruka.Name = "txtPoruka";
            this.txtPoruka.Size = new System.Drawing.Size(607, 86);
            this.txtPoruka.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 502);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Unesite tekst zahteva:";
            // 
            // btnZatrazi
            // 
            this.btnZatrazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZatrazi.Location = new System.Drawing.Point(39, 698);
            this.btnZatrazi.Name = "btnZatrazi";
            this.btnZatrazi.Size = new System.Drawing.Size(220, 46);
            this.btnZatrazi.TabIndex = 4;
            this.btnZatrazi.Text = "Posalji zahtev";
            this.btnZatrazi.UseVisualStyleBackColor = true;
            this.btnZatrazi.Click += new System.EventHandler(this.btnZatrazi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Unesite naziv predemta:";
            // 
            // txtPredmet
            // 
            this.txtPredmet.Location = new System.Drawing.Point(39, 457);
            this.txtPredmet.Name = "txtPredmet";
            this.txtPredmet.Size = new System.Drawing.Size(607, 26);
            this.txtPredmet.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(445, 698);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(203, 46);
            this.button1.TabIndex = 7;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.Paint += new System.Windows.Forms.PaintEventHandler(this.button1_Paint);
            // 
            // ZahtevLiteratura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(702, 762);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPredmet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnZatrazi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPoruka);
            this.Controls.Add(this.dgvZahtevi);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ZahtevLiteratura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZahtevLiteratura";
            this.Load += new System.EventHandler(this.ZahtevLiteratura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvZahtevi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvZahtevi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.TextBox txtPoruka;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnZatrazi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPredmet;
        private System.Windows.Forms.Button button1;
    }
}