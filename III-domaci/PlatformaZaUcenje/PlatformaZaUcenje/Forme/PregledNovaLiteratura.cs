﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class PregledNovaLiteratura : Form
    {
        public PregledNovaLiteratura()
        {
            InitializeComponent();
        }
        public void ucitajPodatke()
        {
            listView1.Items.Clear();

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            //var collection1 = database.GetCollection<Dokument>("dokument");
            //MongoCursor<Dokument> dokument = collection1.FindAll();
            //// MessageBox.Show(dokument.ToJson());




            var collection = database.GetCollection("zahtev");
            // zahtevi = collection.FindAll();

            var zahteviSvi = from zahtev in collection.AsQueryable<ZahteviLiteratura>()
                             where zahtev.tip == "literatura"
                             select zahtev;

            // MessageBox.Show(query1.ElementAt(0).naziv);


            foreach (ZahteviLiteratura r in zahteviSvi)//.ToArray<Zahtev>())
            {

              
                ListViewItem item = new ListViewItem(new string[] { r.tip, r.poruka,r.predmet,r.id.ToString() }); // d.naziv, d.autor, d.predmet, d.opis, r.id.ToString(), d.Id.ToString() });

                        listView1.Items.Add(item);
                  
            }

            listView1.Refresh();
        }

        private void PregledNovaLiteratura_Load(object sender, EventArgs e)
        {
            this.ucitajPodatke();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            var zahtevi = database.GetCollection<Zahtev>("zahtev");
           

            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite zahtev!");
                return;
            }


            string idZahteva = listView1.SelectedItems[0].SubItems[3].Text.ToString();
          

            var query = Query<Zahtev>.EQ(i => i.id, new BsonObjectId(idZahteva));
            zahtevi.Remove(query);
           
            this.ucitajPodatke();
        }

        private void btnIzadji_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIzadji_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
