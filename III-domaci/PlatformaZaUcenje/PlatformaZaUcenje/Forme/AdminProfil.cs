﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlatformaZaUcenje
{
    public partial class AdminProfil : Form
    {
        Admin admin;
        public AdminProfil()
        {
            InitializeComponent();
            
            
        }
        public AdminProfil(Admin a)
        {
            InitializeComponent();
            admin = a;

        }

        private void btnKorisnici_Click(object sender, EventArgs e)
        {
            AdminKorisnici ak = new AdminKorisnici(admin);
            ak.Show();
        }

        private void btnPredavanja_Click(object sender, EventArgs e)
        {
            AdminPredavanja ap = new AdminPredavanja();
            ap.Show();
        }

        private void btnDokumenta_Click(object sender, EventArgs e)
        {
            PregledDokumenataAdmin pda = new PregledDokumenataAdmin();
            pda.Show();
        }

        private void btnZahtevi_Click(object sender, EventArgs e)
        {

        }

        private void AdminProfil_Load(object sender, EventArgs e)
        {
            cmbZahtevi.Text = "Zahtevi";
        }

        private void cmbZahtevi_SelectedIndexChanged(object sender, EventArgs e)
        {
            string zahtev = cmbZahtevi.SelectedItem.ToString();
            if (zahtev == "Neprimeren sadrzaj")
            {
                PregledNeprimerenSadrzaj pns = new PregledNeprimerenSadrzaj();
                pns.ShowDialog();
            }
            else if (zahtev == "Nova literatura")
            {
                PregledNovaLiteratura pns = new PregledNovaLiteratura();
                pns.ShowDialog();
            }
            else if (zahtev == "Nova predavanja")
            {
                PregledNovaPredavanja pns = new PregledNovaPredavanja();
                pns.ShowDialog();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdminProfil_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
