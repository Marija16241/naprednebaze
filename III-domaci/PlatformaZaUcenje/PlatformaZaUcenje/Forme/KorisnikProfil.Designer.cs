﻿namespace PlatformaZaUcenje
{
    partial class KorisnikProfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSadrzaj = new System.Windows.Forms.Button();
            this.btnDokumenta = new System.Windows.Forms.Button();
            this.btnPredavanja = new System.Windows.Forms.Button();
            this.btnProfil = new System.Windows.Forms.Button();
            this.btnZahtevi = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSadrzaj
            // 
            this.btnSadrzaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSadrzaj.Location = new System.Drawing.Point(18, 183);
            this.btnSadrzaj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSadrzaj.Name = "btnSadrzaj";
            this.btnSadrzaj.Size = new System.Drawing.Size(138, 35);
            this.btnSadrzaj.TabIndex = 0;
            this.btnSadrzaj.Text = "Moj sadrzaj";
            this.btnSadrzaj.UseVisualStyleBackColor = true;
            this.btnSadrzaj.Click += new System.EventHandler(this.btnSadrzaj_Click);
            // 
            // btnDokumenta
            // 
            this.btnDokumenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDokumenta.Location = new System.Drawing.Point(18, 242);
            this.btnDokumenta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDokumenta.Name = "btnDokumenta";
            this.btnDokumenta.Size = new System.Drawing.Size(138, 35);
            this.btnDokumenta.TabIndex = 1;
            this.btnDokumenta.Text = "Dokumenta";
            this.btnDokumenta.UseVisualStyleBackColor = true;
            this.btnDokumenta.Click += new System.EventHandler(this.btnDokumenta_Click);
            // 
            // btnPredavanja
            // 
            this.btnPredavanja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPredavanja.Location = new System.Drawing.Point(164, 242);
            this.btnPredavanja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPredavanja.Name = "btnPredavanja";
            this.btnPredavanja.Size = new System.Drawing.Size(165, 35);
            this.btnPredavanja.TabIndex = 2;
            this.btnPredavanja.Text = "Predavanja";
            this.btnPredavanja.UseVisualStyleBackColor = true;
            this.btnPredavanja.Click += new System.EventHandler(this.btnPredavanja_Click);
            // 
            // btnProfil
            // 
            this.btnProfil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfil.Location = new System.Drawing.Point(164, 183);
            this.btnProfil.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnProfil.Name = "btnProfil";
            this.btnProfil.Size = new System.Drawing.Size(165, 35);
            this.btnProfil.TabIndex = 3;
            this.btnProfil.Text = "Moj profil";
            this.btnProfil.UseVisualStyleBackColor = true;
            this.btnProfil.Click += new System.EventHandler(this.btnProfil_Click);
            // 
            // btnZahtevi
            // 
            this.btnZahtevi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZahtevi.Location = new System.Drawing.Point(337, 183);
            this.btnZahtevi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnZahtevi.Name = "btnZahtevi";
            this.btnZahtevi.Size = new System.Drawing.Size(165, 35);
            this.btnZahtevi.TabIndex = 4;
            this.btnZahtevi.Text = "Zahtevana literatura";
            this.btnZahtevi.UseVisualStyleBackColor = true;
            this.btnZahtevi.Click += new System.EventHandler(this.btnZahtevi_Click);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(337, 242);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 35);
            this.button1.TabIndex = 5;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.Paint += new System.Windows.Forms.PaintEventHandler(this.button1_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PlatformaZaUcenje.Properties.Resources.login;
            this.pictureBox1.Location = new System.Drawing.Point(164, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(165, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // KorisnikProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(536, 320);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnZahtevi);
            this.Controls.Add(this.btnProfil);
            this.Controls.Add(this.btnPredavanja);
            this.Controls.Add(this.btnDokumenta);
            this.Controls.Add(this.btnSadrzaj);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "KorisnikProfil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KorisnikProfil";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSadrzaj;
        private System.Windows.Forms.Button btnDokumenta;
        private System.Windows.Forms.Button btnPredavanja;
        private System.Windows.Forms.Button btnProfil;
        private System.Windows.Forms.Button btnZahtevi;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}