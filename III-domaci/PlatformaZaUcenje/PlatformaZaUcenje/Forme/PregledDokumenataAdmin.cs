﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class PregledDokumenataAdmin : Form
    {
        MongoCursor<Dokument> dokumenta;
        public PregledDokumenataAdmin()
        {
            InitializeComponent();
      
        }

        private void PregledDokumenataAdmin_Load(object sender, EventArgs e)
        {
           // cmbFilter.Text = "Naziv";
            listView1.Items.Clear();

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");

           
            var collection = database.GetCollection("dokument");
            // zahtevi = collection.FindAll();

            var blanketi = from dokument in collection.AsQueryable<Blanket>()
                             where dokument.tip == "blanket"
                             select dokument;
            var knjige = from dokument in collection.AsQueryable<Knjiga>()
                             where dokument.tip == "knjiga"
                             select dokument;
           


            foreach (Blanket t in blanketi)//.ToArray<Zahtev>())
            {

                ListViewItem item = new ListViewItem(new string[] {t.id.ToString(), t.predmet,t.format,t.tip,"",t.rok,t.godina,t.tipBlanketa,t.ocena,t.link
                });
                listView1.Items.Add(item);
            }
            foreach (Knjiga t in knjige)//.ToArray<Zahtev>())
            {

                ListViewItem item = new ListViewItem(new string[]  {t.id.ToString(), t.predmet,t.format,t.tip,t.autor,"","","",t.ocena,t.link
                });
                listView1.Items.Add(item);

            }
            
            listView1.Refresh();


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();

           // string filterListe = cmbFilter.SelectedItem.ToString();
          //  MessageBox.Show(filterListe);
          //  this.FiltrirajPoParametru(filterListe);
        }
        private void FiltrirajPoParametru(string filterListe)
        {
            /*
            foreach (Dokument r in dokumenta.ToArray<Dokument>())
            {
                string a = null, p=null, o=null;
                if (r.autor == null)

                    a = "";
                else
                    a = r.autor;

                if (r.predmet == null)
                    p = "";
                else
                    p = r.predmet;
                if (r.opis == null)
                    o = "";
                else
                    o = r.opis;
                if (filterListe == "Naziv")
                {
                    if (r.naziv.ToLower().Contains(txtFilter.Text.ToLower()))
                    {
                        ListViewItem item = new ListViewItem(new string[] { r.naziv, r.autor, r.predmet, r.opis });
                        listView1.Items.Add(item);
                    }
                }
                if (filterListe == "Autor")
                {
                    if (a.ToLower().Contains(txtFilter.Text.ToLower()))
                    {
                        ListViewItem item = new ListViewItem(new string[] { r.naziv, a, p, o });// r.autor, r.predmet, r.opis });
                        listView1.Items.Add(item);
                    }
                }
                //if (filterListe == "Predmet")
                //{
                //    if (p.ToString().ToLower().Equals(txtFilter.Text.ToLower()))
                //    {
                //        ListViewItem item = new ListViewItem(new string[] { r.naziv, a,p,o });
                //        listView1.Items.Add(item);
                //    }
                //}
                listView1.Refresh();
            }
            */

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
