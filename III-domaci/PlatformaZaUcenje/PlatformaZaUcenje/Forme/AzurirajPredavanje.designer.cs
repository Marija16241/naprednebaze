﻿namespace PlatformaZaUcenje
{
    partial class AzurirajPredavanje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPredmet = new System.Windows.Forms.TextBox();
            this.txtVreme = new System.Windows.Forms.TextBox();
            this.txtMesto = new System.Windows.Forms.TextBox();
            this.txtOznake = new System.Windows.Forms.RichTextBox();
            this.txtPrijavljeniKorisnici = new System.Windows.Forms.RichTextBox();
            this.btnAzuriraj = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Predmet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vreme";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 154);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mesto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 214);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Oznake";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 382);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Prijavljeni korisnici";
            // 
            // txtPredmet
            // 
            this.txtPredmet.Location = new System.Drawing.Point(240, 28);
            this.txtPredmet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPredmet.Name = "txtPredmet";
            this.txtPredmet.Size = new System.Drawing.Size(148, 26);
            this.txtPredmet.TabIndex = 5;
            // 
            // txtVreme
            // 
            this.txtVreme.Location = new System.Drawing.Point(240, 86);
            this.txtVreme.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtVreme.Name = "txtVreme";
            this.txtVreme.Size = new System.Drawing.Size(148, 26);
            this.txtVreme.TabIndex = 6;
            // 
            // txtMesto
            // 
            this.txtMesto.Location = new System.Drawing.Point(240, 148);
            this.txtMesto.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMesto.Name = "txtMesto";
            this.txtMesto.Size = new System.Drawing.Size(148, 26);
            this.txtMesto.TabIndex = 7;
            // 
            // txtOznake
            // 
            this.txtOznake.Location = new System.Drawing.Point(141, 214);
            this.txtOznake.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOznake.Name = "txtOznake";
            this.txtOznake.Size = new System.Drawing.Size(247, 146);
            this.txtOznake.TabIndex = 8;
            this.txtOznake.Text = "";
            // 
            // txtPrijavljeniKorisnici
            // 
            this.txtPrijavljeniKorisnici.Location = new System.Drawing.Point(141, 420);
            this.txtPrijavljeniKorisnici.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPrijavljeniKorisnici.Name = "txtPrijavljeniKorisnici";
            this.txtPrijavljeniKorisnici.ReadOnly = true;
            this.txtPrijavljeniKorisnici.Size = new System.Drawing.Size(254, 172);
            this.txtPrijavljeniKorisnici.TabIndex = 11;
            this.txtPrijavljeniKorisnici.Text = "";
            // 
            // btnAzuriraj
            // 
            this.btnAzuriraj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAzuriraj.Location = new System.Drawing.Point(44, 639);
            this.btnAzuriraj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAzuriraj.Name = "btnAzuriraj";
            this.btnAzuriraj.Size = new System.Drawing.Size(150, 41);
            this.btnAzuriraj.TabIndex = 15;
            this.btnAzuriraj.Text = "Azuriraj ";
            this.btnAzuriraj.UseVisualStyleBackColor = true;
            this.btnAzuriraj.Click += new System.EventHandler(this.btnAzuriraj_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(240, 639);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 41);
            this.button1.TabIndex = 16;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AzurirajPredavanje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(436, 694);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAzuriraj);
            this.Controls.Add(this.txtPrijavljeniKorisnici);
            this.Controls.Add(this.txtOznake);
            this.Controls.Add(this.txtMesto);
            this.Controls.Add(this.txtVreme);
            this.Controls.Add(this.txtPredmet);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AzurirajPredavanje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AzurirajPredavanje";
            this.Load += new System.EventHandler(this.AzurirajPredavanje_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AzurirajPredavanje_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPredmet;
        private System.Windows.Forms.TextBox txtVreme;
        private System.Windows.Forms.TextBox txtMesto;
        private System.Windows.Forms.RichTextBox txtOznake;
        private System.Windows.Forms.RichTextBox txtPrijavljeniKorisnici;
        private System.Windows.Forms.Button btnAzuriraj;
        private System.Windows.Forms.Button button1;
    }
}