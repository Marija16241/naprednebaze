﻿namespace PlatformaZaUcenje
{
    partial class AdminProfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKorisnici = new System.Windows.Forms.Button();
            this.btnPredavanja = new System.Windows.Forms.Button();
            this.btnDokumenta = new System.Windows.Forms.Button();
            this.cmbZahtevi = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnKorisnici
            // 
            this.btnKorisnici.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKorisnici.Location = new System.Drawing.Point(32, 24);
            this.btnKorisnici.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnKorisnici.Name = "btnKorisnici";
            this.btnKorisnici.Size = new System.Drawing.Size(182, 35);
            this.btnKorisnici.TabIndex = 0;
            this.btnKorisnici.Text = "Korisnici";
            this.btnKorisnici.UseVisualStyleBackColor = true;
            this.btnKorisnici.Click += new System.EventHandler(this.btnKorisnici_Click);
            // 
            // btnPredavanja
            // 
            this.btnPredavanja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPredavanja.Location = new System.Drawing.Point(32, 90);
            this.btnPredavanja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPredavanja.Name = "btnPredavanja";
            this.btnPredavanja.Size = new System.Drawing.Size(182, 35);
            this.btnPredavanja.TabIndex = 1;
            this.btnPredavanja.Text = "Predavanja";
            this.btnPredavanja.UseVisualStyleBackColor = true;
            this.btnPredavanja.Click += new System.EventHandler(this.btnPredavanja_Click);
            // 
            // btnDokumenta
            // 
            this.btnDokumenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDokumenta.Location = new System.Drawing.Point(256, 24);
            this.btnDokumenta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDokumenta.Name = "btnDokumenta";
            this.btnDokumenta.Size = new System.Drawing.Size(182, 35);
            this.btnDokumenta.TabIndex = 2;
            this.btnDokumenta.Text = "Dokumenta";
            this.btnDokumenta.UseVisualStyleBackColor = true;
            this.btnDokumenta.Click += new System.EventHandler(this.btnDokumenta_Click);
            // 
            // cmbZahtevi
            // 
            this.cmbZahtevi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmbZahtevi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbZahtevi.ForeColor = System.Drawing.SystemColors.Window;
            this.cmbZahtevi.FormattingEnabled = true;
            this.cmbZahtevi.Items.AddRange(new object[] {
            "Neprimeren sadrzaj",
            "Nova literatura",
            "Nova predavanja"});
            this.cmbZahtevi.Location = new System.Drawing.Point(258, 97);
            this.cmbZahtevi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbZahtevi.Name = "cmbZahtevi";
            this.cmbZahtevi.Size = new System.Drawing.Size(180, 28);
            this.cmbZahtevi.TabIndex = 4;
            this.cmbZahtevi.SelectedIndexChanged += new System.EventHandler(this.cmbZahtevi_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(256, 202);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 35);
            this.button1.TabIndex = 5;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AdminProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(478, 254);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbZahtevi);
            this.Controls.Add(this.btnDokumenta);
            this.Controls.Add(this.btnPredavanja);
            this.Controls.Add(this.btnKorisnici);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AdminProfil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminProfil";
            this.Load += new System.EventHandler(this.AdminProfil_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AdminProfil_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnKorisnici;
        private System.Windows.Forms.Button btnPredavanja;
        private System.Windows.Forms.Button btnDokumenta;
        private System.Windows.Forms.ComboBox cmbZahtevi;
        private System.Windows.Forms.Button button1;
    }
}