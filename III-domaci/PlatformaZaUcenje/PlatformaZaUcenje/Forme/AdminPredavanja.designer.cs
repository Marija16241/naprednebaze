﻿namespace PlatformaZaUcenje
{
    partial class AdminPredavanja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvPredavanja = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAzurirajPredavanje = new System.Windows.Forms.Button();
            this.btnObrisiPredavanje = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvPredavanja
            // 
            this.lvPredavanja.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvPredavanja.FullRowSelect = true;
            this.lvPredavanja.HideSelection = false;
            this.lvPredavanja.Location = new System.Drawing.Point(18, 34);
            this.lvPredavanja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lvPredavanja.Name = "lvPredavanja";
            this.lvPredavanja.Size = new System.Drawing.Size(814, 333);
            this.lvPredavanja.TabIndex = 0;
            this.lvPredavanja.UseCompatibleStateImageBehavior = false;
            this.lvPredavanja.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Id";
            this.columnHeader1.Width = 0;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Predmet";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Vreme";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Mesto";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Oznake";
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Prijavljeni korisnici";
            this.columnHeader6.Width = 400;
            // 
            // btnAzurirajPredavanje
            // 
            this.btnAzurirajPredavanje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAzurirajPredavanje.Location = new System.Drawing.Point(18, 389);
            this.btnAzurirajPredavanje.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAzurirajPredavanje.Name = "btnAzurirajPredavanje";
            this.btnAzurirajPredavanje.Size = new System.Drawing.Size(164, 35);
            this.btnAzurirajPredavanje.TabIndex = 1;
            this.btnAzurirajPredavanje.Text = "Azuriraj predavanje";
            this.btnAzurirajPredavanje.UseVisualStyleBackColor = true;
            this.btnAzurirajPredavanje.Click += new System.EventHandler(this.btnAzurirajPredavanje_Click);
            // 
            // btnObrisiPredavanje
            // 
            this.btnObrisiPredavanje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObrisiPredavanje.Location = new System.Drawing.Point(190, 389);
            this.btnObrisiPredavanje.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnObrisiPredavanje.Name = "btnObrisiPredavanje";
            this.btnObrisiPredavanje.Size = new System.Drawing.Size(190, 35);
            this.btnObrisiPredavanje.TabIndex = 2;
            this.btnObrisiPredavanje.Text = "Obrisi predavanje";
            this.btnObrisiPredavanje.UseVisualStyleBackColor = true;
            this.btnObrisiPredavanje.Click += new System.EventHandler(this.btnObrisiPredavanje_Click);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(642, 389);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(190, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AdminPredavanja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(853, 447);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnObrisiPredavanje);
            this.Controls.Add(this.btnAzurirajPredavanje);
            this.Controls.Add(this.lvPredavanja);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AdminPredavanja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminPredavanja";
            this.Load += new System.EventHandler(this.AdminPredavanja_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AdminPredavanja_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvPredavanja;
        private System.Windows.Forms.Button btnAzurirajPredavanje;
        private System.Windows.Forms.Button btnObrisiPredavanje;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button button1;
    }
}