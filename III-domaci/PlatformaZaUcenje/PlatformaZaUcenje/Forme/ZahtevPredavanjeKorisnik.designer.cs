﻿namespace PlatformaZaUcenje
{
    partial class ZahtevPredavanjeKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPosaljiZahtev = new System.Windows.Forms.Button();
            this.txtPredmet = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTrajanje = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOpis = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPosaljiZahtev
            // 
            this.btnPosaljiZahtev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosaljiZahtev.Location = new System.Drawing.Point(23, 316);
            this.btnPosaljiZahtev.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPosaljiZahtev.Name = "btnPosaljiZahtev";
            this.btnPosaljiZahtev.Size = new System.Drawing.Size(150, 35);
            this.btnPosaljiZahtev.TabIndex = 0;
            this.btnPosaljiZahtev.Text = "Posalji zahtev";
            this.btnPosaljiZahtev.UseVisualStyleBackColor = true;
            this.btnPosaljiZahtev.Click += new System.EventHandler(this.btnPosaljiZahtev_Click);
            // 
            // txtPredmet
            // 
            this.txtPredmet.Location = new System.Drawing.Point(182, 43);
            this.txtPredmet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPredmet.Name = "txtPredmet";
            this.txtPredmet.Size = new System.Drawing.Size(148, 26);
            this.txtPredmet.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Predmet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 126);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Vreme";
            // 
            // txtTrajanje
            // 
            this.txtTrajanje.Location = new System.Drawing.Point(182, 120);
            this.txtTrajanje.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTrajanje.Name = "txtTrajanje";
            this.txtTrajanje.Size = new System.Drawing.Size(148, 26);
            this.txtTrajanje.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 205);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mesto";
            // 
            // txtOpis
            // 
            this.txtOpis.Location = new System.Drawing.Point(182, 199);
            this.txtOpis.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOpis.Name = "txtOpis";
            this.txtOpis.Size = new System.Drawing.Size(148, 26);
            this.txtOpis.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(182, 316);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 35);
            this.button1.TabIndex = 7;
            this.button1.Text = "Izadji";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ZahtevPredavanjeKorisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(374, 381);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtOpis);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTrajanje);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPredmet);
            this.Controls.Add(this.btnPosaljiZahtev);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ZahtevPredavanjeKorisnik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZahtevPredavanjeKorisnik";
            this.Load += new System.EventHandler(this.ZahtevPredavanjeKorisnik_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ZahtevPredavanjeKorisnik_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPosaljiZahtev;
        private System.Windows.Forms.TextBox txtPredmet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTrajanje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOpis;
        private System.Windows.Forms.Button button1;
    }
}