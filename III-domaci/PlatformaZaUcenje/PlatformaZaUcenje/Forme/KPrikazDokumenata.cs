﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Net;
namespace PlatformaZaUcenje
{
    public partial class KPrikazDokumenata : Form
    {

        WebClient wc = new WebClient();
        List<ObjectId> dokuments;
        public KPrikazDokumenata()
        {
            InitializeComponent();
            dokuments = new List<ObjectId>();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                groupBox1.Visible = true;
                checkBox2.Checked = false;
            }
            else
            {
                groupBox1.Visible = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                groupBox2.Visible = true;
                checkBox1.Checked = false;


            }
            else
            {
                groupBox2.Visible = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection("dokument");
            listView1.Items.Clear();

            if (checkBox1.Checked == true)

            {//blanketi

                var sviBlanketi = from blanket in collection.AsQueryable<Blanket>()
                                  where blanket.tip == "blanket"
                                  select blanket;
                var pom = sviBlanketi;
                if (textBox1.TextLength != 0)
                {//trazim po imenu

                    var poImenu = from blanket in sviBlanketi.AsQueryable<Blanket>()
                                  where blanket.predmet == textBox1.Text
                                  select blanket;
                    pom = poImenu;
                }

                if (textBox2.TextLength != 0)
                {
                    var poRoku = from blanket in pom.AsQueryable<Blanket>()
                                 where blanket.rok == textBox2.Text
                                 select blanket;
                    pom = poRoku;
                }
                if (comboBox1.SelectedIndex != -1)
                {
                    var poGodini = from blanket in pom.AsQueryable<Blanket>()
                                   where blanket.godina == comboBox1.Text
                                   select blanket;
                    pom = poGodini;
                }
                if (checkBox3.Checked == true)
                {
                    var pismeni = from blanket in pom.AsQueryable<Blanket>()
                                  where blanket.tipBlanketa == "pismeni"
                                  select blanket;
                    pom = pismeni;
                }
                else if (checkBox4.Checked == true)
                {
                    var usmeni = from blanket in pom.AsQueryable<Blanket>()
                                 where blanket.tipBlanketa == "usmeni"
                                 select blanket;
                    pom = usmeni;
                }

                foreach (Blanket t in pom)
                {
                    ListViewItem item = new ListViewItem(new string[] {t.id.ToString(), t.predmet,t.format,t.tip,"",t.rok,t.godina,t.tipBlanketa,t.ocena,t.link
                                    });
                    listView1.Items.Add(item);
                    dokuments.Add(t.id);

                }

            }

            else if (checkBox2.Checked == true)
            {


                var sveKnjige = from knjiga in collection.AsQueryable<Knjiga>()
                                where knjiga.tip == "knjiga"
                                select knjiga;
                var pom = sveKnjige;
                if (textBox1.TextLength != 0)
                {//trazim po imenu

                    var poImenu = from blanket in sveKnjige.AsQueryable<Knjiga>()
                                  where blanket.predmet == textBox1.Text
                                  select blanket;
                    pom = poImenu;
                }

                if (textBox3.TextLength != 0)
                {
                    var poAutoru = from blanket in pom.AsQueryable<Knjiga>()
                                   where blanket.autor == textBox3.Text
                                   select blanket;
                    pom = poAutoru;
                }

                foreach (Knjiga t in pom)
                {

                    ListViewItem item = new ListViewItem(new string[]  {t.id.ToString(), t.predmet,t.format,t.tip,t.autor,"","","",t.ocena,t.link
                            });
                    listView1.Items.Add(item);
                    dokuments.Add(t.id);
                }



                listView1.Refresh();

            }
            else
            {
                var sveKnjige = from knjiga in collection.AsQueryable<Knjiga>()
                                where knjiga.tip == "knjiga"
                                select knjiga;
                var pom = sveKnjige;

                var sB = from knjiga in collection.AsQueryable<Blanket>()
                                where knjiga.tip == "blanket"
                                select knjiga;
                var pomB = sB;
                if (textBox1.TextLength != 0)
                {//trazim po imenu

                    var poImenu = from blanket in sveKnjige.AsQueryable<Knjiga>()
                                  where blanket.predmet == textBox1.Text
                                  select blanket;
                    pom = poImenu;

                    var bI= from blanket in sB.AsQueryable<Blanket>()
                            where blanket.predmet == textBox1.Text
                            select blanket;
                    pomB = bI;
                }
                foreach (Knjiga t in pom)
                {

                    ListViewItem item = new ListViewItem(new string[]  {t.id.ToString(), t.predmet,t.format,t.tip,t.autor,"","","",t.ocena,t.link
                            });
                    listView1.Items.Add(item);
                    dokuments.Add(t.id);
                }
                foreach (Blanket t in pomB)
                {
                    ListViewItem item = new ListViewItem(new string[] {t.id.ToString(), t.predmet,t.format,t.tip,"",t.rok,t.godina,t.tipBlanketa,t.ocena,t.link
                                    });
                    listView1.Items.Add(item);
                    dokuments.Add(t.id);

                }
            }
        }
        private void Ucitaj() {
            string connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            var collection = db.GetCollection("dokument");


            var query1 = from blanket in collection.AsQueryable<Blanket>()
                         where blanket.tip == "blanket"
                         select blanket;

            listView1.Items.Clear();
            foreach (Blanket t in query1)
            {
                ListViewItem item = new ListViewItem(new string[] {t.id.ToString(), t.predmet,t.format,t.tip,"",t.rok,t.godina,t.tipBlanketa,t.ocena,t.link
                });
                listView1.Items.Add(item);
                dokuments.Add(t.id);

            }
            var query2 = from knjiga in collection.AsQueryable<Knjiga>()
                         where knjiga.tip == "knjiga"
                         select knjiga;


            foreach (Knjiga t in query2)
            {

                ListViewItem item = new ListViewItem(new string[]  {t.id.ToString(), t.predmet,t.format,t.tip,t.autor,"","","",t.ocena,t.link
                });
                listView1.Items.Add(item);
                dokuments.Add(t.id);

            }

            listView1.Refresh();
        }
        private void KPrikazDokumenata_Load(object sender, EventArgs e)
        {
            Ucitaj();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite materijal koji zelite da preuzmete.");
                return;
            }
            wc.DownloadFileCompleted += new AsyncCompletedEventHandler(fileDownloadComplite);
            Uri imageUrl = new Uri(listView1.SelectedItems[0].SubItems[9].Text);
            string naziv = listView1.SelectedItems[0].SubItems[1].Text + "." + listView1.SelectedItems[0].SubItems[2].Text;
            wc.DownloadFileAsync(imageUrl, naziv);
            MessageBox.Show("Preuzeli ste materijal. Smesten je u debug folderu po nazivom: " + naziv);
        }
        private void fileDownloadComplite(object sender, AsyncCompletedEventArgs e)
        {
            string naziv = listView1.SelectedItems[0].SubItems[1].Text + "." + listView1.SelectedItems[0].SubItems[2].Text;

          //  MessageBox.Show("Preuzeli ste materijal. Smesten je u debug folderu po nazivom: " + naziv);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox4.Checked==true)
            {
                checkBox3.Checked = false;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox4.Checked = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            textBox3.Text = "";
            Ucitaj();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //oceni
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite materijal koji zelite da ocenite.");
                return;
            }
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            if (listView1.SelectedItems[0].SubItems[3].Text == "blanket")
            {

                var collection = db.GetCollection<Blanket>("dokument");
                var query = Query.EQ("_id", dokuments.ElementAt(listView1.SelectedItems[0].Index));

                Blanket p = collection.Find(query).FirstOrDefault();
                float novaOcena = 0;
                if ( p.ocena != null)
                {
                    novaOcena = (float.Parse(p.ocena) + (float)numericUpDown1.Value) / 2;
                }
                else
                {
                    
                    novaOcena = (float)numericUpDown1.Value;
                }

                var update = MongoDB.Driver.Builders.Update.Set("ocena", novaOcena.ToString());
                collection.Update(query, update);
            }
            else
            {


                var collection = db.GetCollection<Knjiga>("dokument");
                var query = Query.EQ("_id", dokuments.ElementAt(listView1.SelectedItems[0].Index));

                Knjiga p = collection.Find(query).FirstOrDefault();
                float novaOcena = 0;
                if (p.ocena != null)
                {
                    novaOcena = (float.Parse(p.ocena) + (float)numericUpDown1.Value) / 2;
                }
                else
                {
                    
                    novaOcena = (float)numericUpDown1.Value;
                }
            

                var update = MongoDB.Driver.Builders.Update.Set("ocena", novaOcena.ToString());
                collection.Update(query, update);
            }
            
            Ucitaj();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite materijal koji zelite da prijavite.");
                return;
            }

            PrijaviNeprimerenSadrzaj pn = new PrijaviNeprimerenSadrzaj(dokuments.ElementAt(listView1.SelectedItems[0].Index));
            pn.Show();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void KPrikazDokumenata_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }

