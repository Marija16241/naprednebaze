﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;


namespace PlatformaZaUcenje
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");


            var query = Query.And(
                Query.EQ("email", textBox1.Text),
                Query.EQ("sifra", textBox2.Text));

            if (textBox1.Text.Contains("admin"))
            {
                var collection = db.GetCollection<Admin>("korisnik");
                Admin a = collection.Find(query).First();
                if (null == a)
                {
                    MessageBox.Show("Uneli ste nevalidne podatke."); 
                }
                else
                {
                    AdminProfil aP = new AdminProfil(a);
                    aP.Show(); 
                }


            }
            else
            {
                var collection = db.GetCollection<Korisnik>("korisnik");
                
                Korisnik a = collection.Find(query).FirstOrDefault();
                if (null == a)
                {
                    MessageBox.Show("Uneli ste nevalidne podatke.");
                }
                else
                {
                    KorisnikProfil kP = new KorisnikProfil(a);
                    kP.Show();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
