﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class KorisnikPredavanja : Form
    {
        public List<Predavanje> listaPredavanja;
        public Korisnik korisnik;
       // int brojPrijavljivanja = 0;
        public KorisnikPredavanja()
        {
            InitializeComponent();
            listaPredavanja = new List<Predavanje>();
        }

        public KorisnikPredavanja(Korisnik k)
        {
            InitializeComponent();
            listaPredavanja = new List<Predavanje>();
            korisnik = k;
        }

        private void KorisnikPredavanja_Load(object sender, EventArgs e)
        {
            ucitajDataGird();
        }

        private void btnOceni_Click(object sender, EventArgs e)
        {
            int selektovani = dgv.CurrentRow.Index;
           // MessageBox.Show(selektovani.ToString());
            if (selektovani < 0)
            {
                MessageBox.Show("Nijedan dokument nije selektovan");
                return;
            }

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<Predavanje>("predavanje");
            var query = Query.EQ("_id", listaPredavanja.ElementAt(selektovani).id);
            Predavanje p = collection.Find(query).First();

            float novaOcena = 0;
            if (p.ocena != null)
            {
                novaOcena = (float.Parse(p.ocena) + (float)nmu.Value)/2;
            }
            else 
                novaOcena = (float)nmu.Value;

            var update = MongoDB.Driver.Builders.Update.Set("ocena", novaOcena.ToString());
            collection.Update(query, update);

            dgv.Rows.Clear();
            ucitajDataGird();

        }

        public void ucitajDataGird()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<Predavanje>("predavanje");
            var listaPredavanja = collection.FindAll();

            foreach (var p in listaPredavanja)
            {
                this.listaPredavanja.Add(p);
                String oznake = " ";
                foreach (String o in p.oznake)
                {
                    oznake += o + " ";
                }
                dgv.Rows.Add( p.predmet + p.mesto + p.vreme + " ocena: " + p.ocena + " broj prijavljenih: " + p.prijevljeniKorisnici.Count + " dodatne karakteristike: " + oznake);
            }
        }

        private void btnPrijavi_Click(object sender, EventArgs e)
        {


            int selektovani = dgv.CurrentRow.Index;
            // MessageBox.Show(selektovani.ToString());
            if (selektovani < 0)
            {
                MessageBox.Show("Nijedan dokument nije selektovan");
                return;
            }

            if (listaPredavanja.Count == 0)
            {
                MessageBox.Show("Trenutno ne postoje predavanja za koje se mozete prijaviti!");
                return;
            }

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<Predavanje>("predavanje");
            var query = Query.EQ("_id", listaPredavanja.ElementAt(selektovani).id);

            Predavanje p = collection.Find(query).First();
            foreach (MongoDBRef m in p.prijevljeniKorisnici)
            {
                if (m.Id == korisnik.id)
                {
                    MessageBox.Show("Vec ste se prijavili za ovo predavanje.");
                    return;
                }
            }
            p.prijevljeniKorisnici.Add(new MongoDBRef("korisnik", korisnik.id));
            collection.Save(p);
            dgv.Rows.Clear();
            ucitajDataGird();

        }

        private void btnMojaPredavanja_Click(object sender, EventArgs e)
        {
            if (korisnik.predavanja == null)
            {
                MessageBox.Show("Nemate predavanja");
                return;
            }
            MojaPredavanja mp = new MojaPredavanja(korisnik);
            mp.Show();
        }

        private void dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgv.Rows[0].Selected = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZahtevPredavanjeKorisnik zpk = new ZahtevPredavanjeKorisnik(korisnik);
            zpk.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
