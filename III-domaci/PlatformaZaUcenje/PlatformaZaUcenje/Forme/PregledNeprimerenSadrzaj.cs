﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
namespace PlatformaZaUcenje
{
    public partial class PregledNeprimerenSadrzaj : Form
    {
        MongoCursor<Zahtev> zahtevi;
        public string tip;
        public PregledNeprimerenSadrzaj()
        {
            InitializeComponent();
        }

        private void PregledNeprimerenSadrzaj_Load(object sender, EventArgs e)
        {
            
            this.ucitajPodatke();
        }
        public void ucitajPodatke()
        {
            //prikazuje samo one zahteve za koje poostoje dokumenti u listi
            listView1.Items.Clear();

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            var collection1 = database.GetCollection<Dokument>("dokument");
            MongoCursor<Dokument> dokument = collection1.FindAll();
            
            var collection = database.GetCollection("zahtev");
           

            var zahteviSvi = from zahtev in collection.AsQueryable<ZahteviNeprimereniSadrzaj>()
                             where zahtev.tip == "neprimeren"
                             select zahtev;

            var sviBlanketi = from blanket in collection1.AsQueryable<Blanket>()
                             where blanket.tip == "blanket"
                             select blanket;
            var sveKnjige = from knjiga in collection1.AsQueryable<Knjiga>()
                              where knjiga.tip == "knjiga"
                              select knjiga;

            foreach (ZahteviNeprimereniSadrzaj r in zahteviSvi)//.ToArray<Zahtev>())
            {

                //foreach (MongoDBRef d in r.dokument)
                {
                  
                    var dokBlanketi = from blanket in sviBlanketi.AsQueryable<Blanket>()
                                      where  r.dokument.Id.AsObjectId==blanket.id
                                      select blanket;
                    var dokKnjige = from knjiga in sveKnjige.AsQueryable<Knjiga>()
                                      where r.dokument.Id.AsObjectId == knjiga.id
                                      select knjiga;
                  
                    foreach (Blanket b in dokBlanketi)
                    {
                        ListViewItem item = new ListViewItem(new string[] { r.opis, b.link, b.tip, b.predmet, r.id.ToString(), b.id.ToString() });

                        listView1.Items.Add(item);

                    }

              
                    foreach (Knjiga k in dokKnjige)
                    {
                        ListViewItem item = new ListViewItem(new string[] { r.opis, k.link, k.tip, k.predmet, r.id.ToString(), k.id.ToString() });

                        listView1.Items.Add(item);

                    }
                  
                }
            }
            listView1.Refresh();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            var zahtevi = database.GetCollection<Zahtev>("zahtev");
            var dokumenta = database.GetCollection<Dokument>("dokument");
          


            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite zahtev!");
                return;
            }


            string idZahteva = listView1.SelectedItems[0].SubItems[4].Text.ToString();
            string idDok = listView1.SelectedItems[0].SubItems[5].Text.ToString();
          
         //brise sve zahteve ukoliko je isti dokument(ako dva zahteva imaju istu referencu na dokument, brise oba)
         //, a brise naravno i taj dokument iz liste 
            var query = Query<Zahtev>.EQ(i => i.id, new BsonObjectId(idZahteva));
            zahtevi.Remove(query);
            var query1 = Query<Dokument>.EQ(i => i.id, new BsonObjectId(idDok));
            dokumenta.Remove(query1);
            this.ucitajPodatke();
        }

        private void btnIzadji_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PregledNeprimerenSadrzaj_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
    
}
