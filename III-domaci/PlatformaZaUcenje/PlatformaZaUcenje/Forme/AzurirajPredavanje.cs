﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class AzurirajPredavanje : Form
    {
        public AdminPredavanja ap;
        public string id;
        List<Korisnik> korisniciZaDodavanje;
        List<MongoDBRef> reference = new List<MongoDBRef>();
        public AzurirajPredavanje()
        {
            InitializeComponent();
            korisniciZaDodavanje = new List<Korisnik>();
        }
        public AzurirajPredavanje(AdminPredavanja ap)
        {
            InitializeComponent();
            this.ap = ap;
        }
        public AzurirajPredavanje(string id)
        {
            InitializeComponent();
            korisniciZaDodavanje = new List<Korisnik>();
            this.id = id;
            this.popuniInformacijama();
            
        }

        private void AzurirajPredavanje_Load(object sender, EventArgs e)
        {

        }
        public void popuniInformacijama()
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Predavanje>("predavanje");

            var queryPredavanja = Query.EQ("_id", ObjectId.Parse(this.id));

            MongoCursor<Predavanje> pred = collection.Find(queryPredavanja);

            var collection1 = database.GetCollection<Korisnik>("korisnik");

           // MongoCursor<Korisnik> korisnici = collection1.FindAll();
            // MongoCursor<Korisnik> korisnici = collection1.FindAll();
            foreach (Predavanje p in pred.ToArray<Predavanje>())
            {
                txtPredmet.Text = p.predmet;
                txtVreme.Text = p.vreme;
                txtMesto.Text = p.mesto;
                if (p.oznake != null)
                {
                    foreach (string o in p.oznake)
                    {
                        txtOznake.AppendText(o + "\r\n");
                        txtOznake.ScrollToCaret();
                    }
                }
                if (p.prijevljeniKorisnici != null)
                {
                    for (int i = 0; i < p.prijevljeniKorisnici.Count; i++)
                    {
                        ObjectId idPom = ObjectId.Parse(p.prijevljeniKorisnici[i].Id.ToString());
                        var query = (from korisnik in collection1.AsQueryable<Korisnik>()
                                     where korisnik.id == idPom
                                     select korisnik);

                                Korisnik k = query.FirstOrDefault();
                      
                                korisniciZaDodavanje.Add(k);
                                txtPrijavljeniKorisnici.AppendText(k.ime + " " + k.prezime + "\r\n");
                                txtPrijavljeniKorisnici.ScrollToCaret();
                            
                        }
                    }
                }
            }
        

        private void btnDodajKorisnika_Click(object sender, EventArgs e)
        {
           

        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Predavanje>("predavanje");

            var queryPredavanja = Query.EQ("_id", ObjectId.Parse(this.id));

            List<string> listaZaUpdate = new List<string>();
            for (int i = 0; i < txtOznake.Lines.Count(); i++)
            {
                listaZaUpdate.Add(txtOznake.Lines[i]);
            }
            // Predavanje pred = collection.Find(queryPredavanja).FirstOrDefault<Predavanje>();
            foreach(Korisnik k in korisniciZaDodavanje)
            {
                reference.Add(new MongoDBRef("korisnik", k.id));
            }
            //var update = MongoDB.Driver.Builders.Update.Set("oznake", BsonValue.Create(new List<string> { "test" }));
            var update = MongoDB.Driver.Builders.Update.Set("predmet", txtPredmet.Text)
                .Set("vreme", txtVreme.Text)
                .Set("mesto", txtMesto.Text)
                .Set("oznake", BsonValue.Create(listaZaUpdate));
                //.Set("prijevljeniKorisnici",BsonTypeMapper.MapToBsonValue(reference));
               //foreach korisnik in lista korisnika update prijevljeniKorisnici


            collection.Update(queryPredavanja, update);

            this.DialogResult = DialogResult.OK;
            this.Close();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AzurirajPredavanje_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
