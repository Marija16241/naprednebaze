﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class ZahtevPredavanjeKorisnik : Form
    {
        Korisnik korisnik;
        public ZahtevPredavanjeKorisnik()
        {
            InitializeComponent();
        }
        public ZahtevPredavanjeKorisnik(Korisnik k)
        {
            korisnik = k;
            InitializeComponent();
        }
        private void btnPosaljiZahtev_Click(object sender, EventArgs e)
        {
            


            string p = txtPredmet.Text;
            string t = txtTrajanje.Text;
            string o = txtOpis.Text;

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            var zahtevi = db.GetCollection<Zahtev>("zahtev");
            var korisnici = db.GetCollection<Korisnik>("korisnik");

            ZahteviPredavanje z = new ZahteviPredavanje { tip = "predavanje", predmet = p, vreme = t, mesto = o };

            zahtevi.Insert(z);

            var query = Query.EQ("email", korisnik.email);
            var k = korisnici.Find(query);
          
            Korisnik j = k.ElementAt(0);

            // z.Korisnik1.Add(new Korisnik { Id=j.Id, ime="jelena"} );//new MongoDBRef("korisnik", j.Id));// ToBsonDocument()));//radiii
            z.predavac=new MongoDBRef("korisnik", j.id);

            zahtevi.Save(z);
            MessageBox.Show("Zahtev je prosledjen adminu i ceka odobrenje");
            this.Close();

        }

        private void ZahtevPredavanjeKorisnik_Load(object sender, EventArgs e)
        {

        }

        private void ZahtevPredavanjeKorisnik_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
