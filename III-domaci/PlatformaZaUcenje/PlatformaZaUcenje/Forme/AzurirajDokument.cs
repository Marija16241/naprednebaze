﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class AzurirajDokument : Form
    {
        MongoDBRef refDokumenta;
        String tip;
        public AzurirajDokument()
        {
            InitializeComponent();
        }
        public AzurirajDokument(MongoDBRef refDokumenta)
        {
            InitializeComponent();
            txtTip.Enabled = false;
            txtPredmet.Enabled = false;

            this.refDokumenta = refDokumenta;

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collectionDokument = db.GetCollection<Dokument>("dokument");
            MongoCursor<Dokument> dokument = collectionDokument.FindAll();

            var sviBlanketi = from blanket in collectionDokument.AsQueryable<Blanket>()
                              where blanket.tip == "blanket"
                              select blanket;
            var sveKnjige = from knjiga in collectionDokument.AsQueryable<Knjiga>()
                            where knjiga.tip == "knjiga"
                            select knjiga;

            var dokBlanketi = from blanket in sviBlanketi.AsQueryable<Blanket>()
                              where refDokumenta.Id.AsObjectId == blanket.id
                              select blanket;
            var dokKnjige = from knjiga in sveKnjige.AsQueryable<Knjiga>()
                            where refDokumenta.Id.AsObjectId == knjiga.id
                            select knjiga;
            //popunjavanje forme
            if (dokKnjige.ToJson() != "[]")
            {
                popuniFormuKnjige(dokKnjige.First());
                tip = "knjiga";
            }
            else
            {
                popuniFormuBlanketa(dokBlanketi.First());
                tip = "blanket";
            }
                

        }
        void popuniFormuKnjige(Knjiga k)
        {
            txtTip.Text = k.tip;
            txtFormat.Text = k.format;
            txtLink.Text = k.link;
            txtPredmet.Text = k.predmet; 
            txtOpis.Text = k.opis;
            txtAutor.Text = k.autor;
            foreach (String s in k.oznake)
                dgvPodaci.Rows.Add(s);
            panel.Enabled = false;
        }

        void popuniFormuBlanketa(Blanket b)
        {
            txtTip.Text = b.tip;
            txtFormat.Text = b.format;
            txtLink.Text = b.link;
            txtPredmet.Text = b.predmet;
            txtRok.Text = b.rok;
            
            foreach(String s in cmbGodina.Items)
            {
                if (s.Equals(b.godina))
                    cmbGodina.Text = s;
            }

            foreach (String s in cmbTipBlanketa.Items)
            {
                if (s.Equals(b.tipBlanketa))
                    cmbTipBlanketa.Text = s;
            }

            foreach (String s in b.oznake)
                dgvPodaci.Rows.Add(s);

            panel1.Enabled = false;
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            var collection = db.GetCollection<Dokument>("dokument");
            var query = Query.EQ("_id", refDokumenta.Id.AsObjectId);

            List<String> listaIzDataGrida = new List<string>();
            foreach (DataGridViewRow item in dgvPodaci.Rows)
            {
                if (item.Cells[0].Value != null)
                {
                    listaIzDataGrida.Add(item.Cells[0].Value.ToString());
                }
            }
            var update = MongoDB.Driver.Builders.Update.Set("oznake", BsonValue.Create(listaIzDataGrida));
            collection.Update(query, update);

            var updateLink = MongoDB.Driver.Builders.Update.Set("link", txtLink.Text);
            collection.Update(query, updateLink);

            var updateFormat = MongoDB.Driver.Builders.Update.Set("format", txtFormat.Text);
            collection.Update(query, updateFormat);

            if(tip.Equals("knjiga"))
            {
                var updateAutor = MongoDB.Driver.Builders.Update.Set("autor", txtAutor.Text);
                collection.Update(query, updateAutor);

                var updateOpis = MongoDB.Driver.Builders.Update.Set("opis", txtOpis.Text);
                collection.Update(query, updateOpis);
            }
            else
            {
                var updateRok = MongoDB.Driver.Builders.Update.Set("rok", txtRok.Text);
                collection.Update(query, updateRok);

                var updateGodina = MongoDB.Driver.Builders.Update.Set("godina", cmbGodina.Text);
                collection.Update(query, updateGodina);

                var updateTipBlanketa = MongoDB.Driver.Builders.Update.Set("tipBlanketa", cmbTipBlanketa.Text);
                collection.Update(query, updateTipBlanketa);
            }
            

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AzurirajDokument_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
