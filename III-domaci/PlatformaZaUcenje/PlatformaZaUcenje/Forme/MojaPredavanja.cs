﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;


namespace PlatformaZaUcenje
{
    public partial class MojaPredavanja : Form
    {
        public Korisnik k ;
        public List<String> listaIDjeva = new List<String>();
        public List<Predavanje> listaPredavanja = new List<Predavanje>();
        
        public MojaPredavanja()
        {
            InitializeComponent();
        }
        public MojaPredavanja(Korisnik kk)
        {
            k = kk;
            InitializeComponent();
        }
        public void pribaviPredavanjaZaKorisnika()
        {
            
            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

           // var collectionKorisnici = database.GetCollection<Korisnik>("korisnik");

            //Korisnik k = collectionKorisnici.FindAll().FirstOrDefault<Korisnik>();

            var collectionPredavanja = database.GetCollection<Predavanje>("predavanje");

           
            foreach(MongoDBRef referencaNaPredavanje in k.predavanja.ToList())
            {
                listaIDjeva.Add(referencaNaPredavanje.Id.ToString());
            }
            foreach(String idPredavanja in listaIDjeva)
            {
                var query = Query.EQ("_id", ObjectId.Parse(idPredavanja));
                listaPredavanja.Add(collectionPredavanja.Find(query).FirstOrDefault<Predavanje>());
            }
           // MessageBox.Show(listaPredavanja.ToJson());
            foreach(Predavanje p in listaPredavanja)
            {
                String listaOznaka = "";
                if(p.oznake==null)
                {
                    p.oznake = new List<string>();
                }
                foreach(String s in p.oznake)
                {
                    listaOznaka += s + " ";
                }
                ListViewItem item = new ListViewItem(new string[] { p.id.ToString(), p.predmet, p.mesto, p.vreme, listaOznaka });
                listView1.Items.Add(item);
                
            }
        }

        private void MojaPredavanja_Load(object sender, EventArgs e)
        {
            pribaviPredavanjaZaKorisnika();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MojaPredavanja_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
