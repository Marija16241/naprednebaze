﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class PregledNovaPredavanja : Form
    {
        public PregledNovaPredavanja()
        {
            InitializeComponent();
        }
        public void ucitajPodatke()
        {
            listView1.Items.Clear();

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            var collection = database.GetCollection("zahtev");
           
            var zahteviSvi = from zahtev in collection.AsQueryable<ZahteviPredavanje>()
                             where zahtev.tip == "predavanje"
                             select zahtev;




            /* foreach (Zahtev r in zahteviSvi)//.ToArray<Zahtev>())
              {


                  foreach (MongoDBRef k in r.korisnik)
                  {
                      Korisnik k1 = database.FetchDBRefAs<Korisnik>(k);

                      ListViewItem item = new ListViewItem(new string[] { r.naziv, r.predmet, r.trajanje, r.opis,k1.ime,k1.prezime,r.id.ToString(),k1.Id.ToString()}); // d.naziv, d.autor, d.predmet, d.opis, r.id.ToString(), d.Id.ToString() });

                      listView1.Items.Add(item);
                  }
              }*/


            foreach (ZahteviPredavanje r in zahteviSvi)//.ToArray<Zahtev>())
            {
               // MessageBox.Show(r.predmet + "--"+r.predavac.Id);
                Korisnik k1 = database.FetchDBRefAs<Korisnik>(r.predavac);
                if (k1 != null)
                { ListViewItem item = new ListViewItem(new string[] { r.tip, r.predmet, r.vreme, r.mesto, k1.ime, k1.prezime, r.id.ToString(), k1.id.ToString() }); // d.naziv, d.autor, d.predmet, d.opis, r.id.ToString(), d.Id.ToString() });

                    listView1.Items.Add(item); 
                }
               
            }
            listView1.Refresh();
        }
        private void PregledNovaPredavanja_Load(object sender, EventArgs e)
        {
            ucitajPodatke();
        }

        private void btnObrisi_Click(object sender, EventArgs e)//ovo je btnDodaj
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("platformazaucenje");



            var zahtevi = database.GetCollection<Zahtev>("zahtev");
            var predavanja = database.GetCollection<Predavanje>("predavanje");
            var korisnici = database.GetCollection<Korisnik>("korisnik");


            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite zahtev!");
                return;
            }

            string idZahteva = listView1.SelectedItems[0].SubItems[6].Text.ToString();
            string idKor = listView1.SelectedItems[0].SubItems[7].Text.ToString();

           

            var query = Query<Zahtev>.EQ(i => i.id, new BsonObjectId(idZahteva));
            string p = listView1.SelectedItems[0].SubItems[1].Text.ToString();
            string t = listView1.SelectedItems[0].SubItems[2].Text.ToString();
            string o = listView1.SelectedItems[0].SubItems[3].Text.ToString();

            Predavanje p1 = new Predavanje { predmet = p, vreme = t, mesto = o };
            predavanja.Insert(p1);
            //nakon dodavanja obrisati iz zahteva i osveziti listu

            var query1 = Query<Zahtev>.EQ(i => i.id, new BsonObjectId(idZahteva));
            zahtevi.Remove(query1);
            
            var k1 = Query<Zahtev>.EQ(i => i.id, new BsonObjectId(idKor));
            Korisnik korisnik = korisnici.Find(k1).FirstOrDefault();

            korisnik.predavanja.Add(new MongoDBRef("predavanje", p1.id));
            korisnici.Save(korisnik);
            this.ucitajPodatke();
            MessageBox.Show("Uspesno dodato predavanje");
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnIzadji_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PregledNovaPredavanja_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
