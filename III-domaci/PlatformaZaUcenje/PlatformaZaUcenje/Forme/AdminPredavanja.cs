﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class AdminPredavanja : Form
    {
        public AdminPredavanja()
        {
            InitializeComponent();
            this.pribaviSvaPredavanja();
        }
        public void pribaviSvaPredavanja()
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Predavanje>("predavanje");


            
           var collection1 = database.GetCollection<Korisnik>("korisnik");

           var korisnici = (from korisnik in collection1.AsQueryable<Korisnik>()
                              select korisnik);
           // MessageBox.Show(korisnici.ElementAt(1).ToJson());


            // MongoCursor<Predavanje> predavanja = collection.FindAll();
            var predavanja = (from predavanje in collection.AsQueryable<Predavanje>()
                              select predavanje);

            lvPredavanja.Items.Clear();

            foreach (Predavanje p in predavanja.ToArray<Predavanje>())
            {
                //List<string> imeprezime = new List<string>();
                string imeprezime = "";
                string oznakeStr = "";
                if (p.oznake == null)
                    p.oznake = new List<string>();
                foreach(string s in p.oznake)
                {
                    oznakeStr += s + " ";
                }

                for (int i = 0; i < p.prijevljeniKorisnici.Count; i++)
                {
                    ObjectId idPom = ObjectId.Parse(p.prijevljeniKorisnici[i].Id.ToString()); 
                  
                   // foreach (Korisnik k in korisnici.ToList())//.ToArray<Korisnik>())
                   var query =  (from korisnik in collection1.AsQueryable<Korisnik>()
                                 where korisnik.id==idPom
                                 select korisnik);
                   //if (query != null)
                   // {
                        Korisnik k = query.FirstOrDefault();
                        imeprezime += k.ime + " " + k.prezime + ", ";
                  //  }
                      
                }

                ListViewItem item = new ListViewItem(new string[] { p.id.ToString(), p.predmet, p.vreme, p.mesto, oznakeStr, imeprezime });
                    lvPredavanja.Items.Add(item);
                
            }

            lvPredavanja.Refresh();
        }

        private void btnObrisiPredavanje_Click(object sender, EventArgs e)
        {
            if (lvPredavanja.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite korisnika!");
                return;
            }

            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Predavanje>("predavanje");

            MongoCursor<Predavanje> predavanja = collection.FindAll();
            foreach (ListViewItem item in lvPredavanja.SelectedItems)
            {
                String id = item.Text;
                //MessageBox.Show(id);
                ObjectId oi = ObjectId.Parse(id);
                collection.Remove(Query.EQ("_id", oi));
            }
            this.pribaviSvaPredavanja();
        }

        private void btnAzurirajPredavanje_Click(object sender, EventArgs e)
        {
            if (lvPredavanja.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite korisnika!");
                return;
            }
            String id = lvPredavanja.SelectedItems[0].Text;
            
             AzurirajPredavanje azuriraj = new AzurirajPredavanje(id);
            azuriraj.ShowDialog();
            if (azuriraj.DialogResult == DialogResult.OK)
            {
                this.pribaviSvaPredavanja();
            } 
        }

        private void AdminPredavanja_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdminPredavanja_Load(object sender, EventArgs e)
        {
            this.pribaviSvaPredavanja();

        }
    }
}
