﻿namespace PlatformaZaUcenje
{
    partial class KorisnikPredavanja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.predavanja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOceni = new System.Windows.Forms.Button();
            this.nmu = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMojaPredavanja = new System.Windows.Forms.Button();
            this.btnPrijavi = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmu)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.predavanja});
            this.dgv.Location = new System.Drawing.Point(30, 54);
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersWidth = 51;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.Size = new System.Drawing.Size(823, 314);
            this.dgv.TabIndex = 0;
            this.dgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_DataBindingComplete);
            // 
            // predavanja
            // 
            this.predavanja.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.predavanja.HeaderText = "Predavanja";
            this.predavanja.MinimumWidth = 6;
            this.predavanja.Name = "predavanja";
            // 
            // btnOceni
            // 
            this.btnOceni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOceni.Location = new System.Drawing.Point(514, 401);
            this.btnOceni.Name = "btnOceni";
            this.btnOceni.Size = new System.Drawing.Size(182, 33);
            this.btnOceni.TabIndex = 1;
            this.btnOceni.Text = "Oceni predavanje";
            this.btnOceni.UseVisualStyleBackColor = true;
            this.btnOceni.Click += new System.EventHandler(this.btnOceni_Click);
            // 
            // nmu
            // 
            this.nmu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nmu.ForeColor = System.Drawing.SystemColors.Window;
            this.nmu.Location = new System.Drawing.Point(376, 401);
            this.nmu.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nmu.Name = "nmu";
            this.nmu.Size = new System.Drawing.Size(132, 26);
            this.nmu.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 403);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Izaberite ocenu za selektovano predavanje:";
            // 
            // btnMojaPredavanja
            // 
            this.btnMojaPredavanja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMojaPredavanja.Location = new System.Drawing.Point(245, 472);
            this.btnMojaPredavanja.Name = "btnMojaPredavanja";
            this.btnMojaPredavanja.Size = new System.Drawing.Size(212, 49);
            this.btnMojaPredavanja.TabIndex = 5;
            this.btnMojaPredavanja.Text = "Moja predavanja";
            this.btnMojaPredavanja.UseVisualStyleBackColor = true;
            this.btnMojaPredavanja.Click += new System.EventHandler(this.btnMojaPredavanja_Click);
            // 
            // btnPrijavi
            // 
            this.btnPrijavi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrijavi.Location = new System.Drawing.Point(39, 472);
            this.btnPrijavi.Name = "btnPrijavi";
            this.btnPrijavi.Size = new System.Drawing.Size(200, 49);
            this.btnPrijavi.TabIndex = 6;
            this.btnPrijavi.Text = "Prijavi se za predavanje";
            this.btnPrijavi.UseVisualStyleBackColor = true;
            this.btnPrijavi.Click += new System.EventHandler(this.btnPrijavi_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(464, 472);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(199, 49);
            this.button1.TabIndex = 7;
            this.button1.Text = "Posalji zahtev";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(671, 472);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(182, 49);
            this.button2.TabIndex = 8;
            this.button2.Text = "Izadji";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.Paint += new System.Windows.Forms.PaintEventHandler(this.button2_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(403, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Predavanja na kojima je ulogovani korisnik predavac:";
            // 
            // KorisnikPredavanja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(915, 561);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnPrijavi);
            this.Controls.Add(this.btnMojaPredavanja);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nmu);
            this.Controls.Add(this.btnOceni);
            this.Controls.Add(this.dgv);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "KorisnikPredavanja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KorisnikPredavanja";
            this.Load += new System.EventHandler(this.KorisnikPredavanja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn predavanja;
        private System.Windows.Forms.Button btnOceni;
        private System.Windows.Forms.NumericUpDown nmu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMojaPredavanja;
        private System.Windows.Forms.Button btnPrijavi;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
    }
}