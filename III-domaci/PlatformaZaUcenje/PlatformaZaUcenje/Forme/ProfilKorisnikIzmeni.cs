﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class ProfilKorisnikIzmeni : Form
    {
        public Korisnik korisnik;
        public ProfilKorisnikIzmeni()
        {
            InitializeComponent();
        }
        public ProfilKorisnikIzmeni(Korisnik k)
        {
            this.korisnik = k;
            InitializeComponent();

        }
        private void ProfilKorisnikIzmeni_Load(object sender, EventArgs e)
        {
            txtIme.Text = korisnik.ime;
            txtPrezime.Text = korisnik.prezime;
            txtSifra.Text = korisnik.sifra;
            txtTelefon.Text = korisnik.telefon;
            textBox1.Text = korisnik.email ;

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {

            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

          // var zahtevi = db.GetCollection<Zahtev>("zahtev");
            var korisnici = db.GetCollection<Korisnik>("korisnik");

            var query = Query.EQ("email", korisnik.email);
            var u1 = MongoDB.Driver.Builders.Update.Set("ime", txtIme.Text);
            var u2 = MongoDB.Driver.Builders.Update.Set("prezime", txtPrezime.Text);
            var u3 = MongoDB.Driver.Builders.Update.Set("telefon", txtTelefon.Text);
            var u4 = MongoDB.Driver.Builders.Update.Set("sifra", txtSifra.Text);
            //var update = Update<Symbol>.Set(e => e.Name, "abc"); // update modifiers  
            //symbolcollection.Update(query3, update);

            korisnici.Update(query, u1);
            korisnici.Update(query, u2);
            korisnici.Update(query, u3);
            korisnici.Update(query, u4);
            MessageBox.Show("Izvrsene su izmene podataka.");
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIzmeni_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
