﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;


namespace PlatformaZaUcenje
{
    public partial class ZahtevLiteratura : Form
    {
        public ZahtevLiteratura()
        {
            InitializeComponent();
        }

        private void ZahtevLiteratura_Load(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");
            var collection = db.GetCollection<ZahteviLiteratura>("zahtev");
            var query = Query.EQ("tip", "literatura");
            var zahtevi = collection.Find(query);
            foreach(ZahteviLiteratura z in zahtevi)
            {
                dgvZahtevi.Rows.Add(z.predmet + ": " + z.poruka);
            }
        }

        private void btnZatrazi_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("platformazaucenje");

            if(txtPoruka.Text == "" || txtPredmet.Text == "")
            {
                MessageBox.Show("Morate uneti naziv predemta i poruku.");
                return;
            }

            ZahteviLiteratura zahtev = new ZahteviLiteratura {tip = "literatura", predmet = txtPredmet.Text, poruka = txtPoruka.Text };

            var collection = db.GetCollection<Zahtev>("zahtev");
            collection.Insert(zahtev);
            dgvZahtevi.Rows.Add(zahtev.predmet + ": " + zahtev.poruka);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }
    }
}
