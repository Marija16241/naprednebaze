﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace PlatformaZaUcenje
{
    public partial class AdminKorisnici : Form
    {
        Admin admin;
        public AdminKorisnici()
        {
            InitializeComponent();
            this.pribaviSveKorisnike();
        }
        public AdminKorisnici(Admin a)
        {
            admin = a;
            InitializeComponent();
            this.pribaviSveKorisnike();
        }
        public void pribaviSveKorisnike()
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Korisnik>("korisnik");

            lvKorisnici.Items.Clear();
            if (admin.korisnici == null)
                return;
            foreach(MongoDBRef m in admin.korisnici)
            {
                var query = Query.EQ("_id", m.Id);
                Korisnik k = collection.Find(query).FirstOrDefault();
                ListViewItem item = new ListViewItem(new string[] { k.id.ToString(), k.ime, k.prezime, k.telefon, k.email });
                lvKorisnici.Items.Add(item);
            }

            lvKorisnici.Refresh();
        }
        private void btnDodajKorisnika_Click(object sender, EventArgs e)
        {
            DodajKorisnika dk = new DodajKorisnika(this, admin);
            dk.ShowDialog();
            if(dk.DialogResult==DialogResult.OK)
            {
                this.pribaviSveKorisnike();
            }
        }

        private void btnObrisiKorisnika_Click(object sender, EventArgs e)
        {
            if (lvKorisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite korisnika!");
                return;
            }

            var connectionString = "mongodb://localhost/?safe=true";

            var server = MongoServer.Create(connectionString);

            var database = server.GetDatabase("platformazaucenje");

            var collection = database.GetCollection<Korisnik>("korisnik");
           
            var collectionPredavanja = database.GetCollection<Predavanje>("predavanje");
            var svaPredavanje = collectionPredavanja.FindAll();
            //

            MongoCursor<Korisnik> korisnici = collection.FindAll();
            foreach(ListViewItem item in lvKorisnici.SelectedItems)
            {
                String id = item.Text;
                ObjectId oi = ObjectId.Parse(id);
                foreach (MongoDBRef mr in admin.korisnici.ToList())
                {
                    if (mr.Id.ToString() == id)
                    {
                        admin.korisnici.Remove(mr);
                    }
                }

               
                foreach (Predavanje p in svaPredavanje)
                {
                    foreach (MongoDBRef m in p.prijevljeniKorisnici.ToList())
                    {
                        if (m.Id.ToString() == id)
                        {
                            p.prijevljeniKorisnici.Remove(m);
                            collectionPredavanja.Save(p);
                        }
                    }
                }
               

                var collectionZahtev = database.GetCollection("zahtev");

                var zahteviSvi = from zahtev in collectionZahtev.AsQueryable<ZahteviPredavanje>()
                                 where zahtev.tip == "predavanje"
                                 select zahtev;

                foreach (ZahteviPredavanje zp in zahteviSvi)
                {
                    if (zp.predavac.Id.ToString() == id)
                    {
                        DialogResult dialogResult = MessageBox.Show("Brisanjem korisnika brisete i zahtev za predavanjem koji je on podneo. Da li zelite da nastavite brisanje?", "Brisanje korisnika", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            collectionZahtev.Remove(Query.EQ("_id", zp.id));
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
               
                collection.Remove(Query.EQ("_id", oi));
              
                collection.Save(admin);
            }

            this.pribaviSveKorisnike();

        }

        private void AdminKorisnici_Load(object sender, EventArgs e)
        {
            this.pribaviSveKorisnike();
        }

        private void AdminKorisnici_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                           this.DisplayRectangle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
